/* ********************************************************************
 *
 * CERN Test 0 Magnet
 *	Compute static solution of Test 0 magnet
 * 	
 * 	Author: Jorge Miaja Hernandez
 *
 ******************************************************************** */
 

// FLAGS AND CONSTANTS
DefineConstant[
//----------------------------- FLAGS ----------------------------------
	// Determines whether statics or dynamics
	 // Used in .geo and .pro files
DefineConstant[
	Flag_magnetName = {1, Choices{0="T0 Magnet", 1="T1 Magnet"}, 
		Name "Flags/Choose test magnet"},	
	Flag_infinityExpansion = {1, Choices{0, 1}, 
		Name "Flags/Infinity mapping region"}
];


