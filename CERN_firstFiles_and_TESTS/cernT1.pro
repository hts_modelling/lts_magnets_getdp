/* ********************************************************************
 *
 * CERN Test 1 Magnet
 *	Compute static solution of Test 1 magnet
 * 	
 * 	Author: Jorge Miaja Hernandez
 *
 ******************************************************************** */
Include "cernT1_common.geo";

Group{ // Abstract regions used in the formulation of the problem
  // Physical regions
  Air = Region[ Air ];
  Coils = Region[ coils_surface ];
  Shield = Region[ ironYoke ];
  Surface_ht0 = Region[ xaxis_ht0Neu ];
  Surface_bn0 = Region[ yaxis_bn0Dir ];

  // Abstract regions
//  Vol_tot = Region[ {Air, Coils, Shield} ];
  Vol_source = Region[ Coils ];
  Sur_Dir = Region[ Surface_bn0 ];
  Sur_Neu = Region[ {} ];

  // Iwth infinity region
  If(Flag_infinityExpansion)
	Surface_Inf = Region[ surfInf ];
	AirInf = Region [ AirInf ];
	Vol_tot = Region[ {Air, AirInf, Coils, Shield} ];
	Vol_inf = Region[ AirInf ];
  Else
	Vol_tot = Region[ {Air, Coils, Shield} ];
  EndIf
}



Function{ // Material laws
  mu0 = 4.e-7 * Pi;
  murIronYolk = 1; // Found in CERN's note crosscheck

  nu [ Region[{Air, Coils}] ]  = 1. / mu0;
  nu [ Shield ]  = 1. / (murIronYolk * mu0);
  If(Flag_infinityExpansion)
	nu [ AirInf ]  = 1. / mu0;
  EndIf

  Current = 0.01; // Found in CERN's note crosscheck
  jsz[ Coils ] = -Current;
}



Constraint{ // Boundary conditions
  { Name Dir_a;
    Case {
      { Region Sur_Dir; Value 0; }
    }
  }
  { Name Source_jDensZ;
    Case {
      { Region Vol_source; Value jsz[]; }
    }
  }
}



Group{ // Function space domain
  Dom_VecPotA = Region[ {Vol_tot, Sur_Neu} ];
}



FunctionSpace{ // where the solution is defined - FE expansion!!
   { Name VecPotA; Type Form1P;
       BasisFunction{ { // a(x,y) = sum_k aCoef_k sBF_k(x,y)
            Name sBF; NameOfCoef aCoef; Function BF_PerpendicularEdge;
            Support Dom_VecPotA; Entity NodesOf[All];
       } }
       Constraint{ { 
            NameOfCoef aCoef; EntityType NodesOf; NameOfConstraint Dir_a;
       } }
   }


   { Name j_region; Type Vector; 
       BasisFunction{ { // js(x,y) = sum_k jsCoef_k jBF_(x,y)
            Name jBF; NameOfCoef jsCoef; Function BF_RegionZ;
	    Support Vol_source; Entity Vol_source;
       } }
       Constraint { { 
	    NameOfCoef jsCoef; EntityType Region;
 	    NameOfConstraint Source_jDensZ;
       } }
   }

}



Jacobian{ // mapping between mesh and reference elements
   { Name Vol;
	Case{ 
	    If(Flag_infinityExpansion)	    
		{ Region All; Jacobian Vol; }
		{ Region Vol_inf; Jacobian VolSphShell {rInf, rInfExt}; }
	    Else
		{ Region All; Jacobian Vol; }
	    EndIf
	} 
   }

   { Name Sur; // if non-homogeneous Neumann boundary conditions
	Case{ {
	   Region All; Jacobian Sur;
	} }
   }
}



Integration{ // Tell GetDP how to compute integrations
  { Name Int ;
    Case { { Type Gauss ;
             Case { { GeoElement Line        ; NumberOfPoints  4 ; }
		    { GeoElement Triangle    ; NumberOfPoints  4 ; }
                    { GeoElement Quadrangle  ; NumberOfPoints  4 ; }
	}
      }
    }
  }
}



Formulation{ // FEM formulation of the problem
   { Name AMagneto; Type FemEquation; 
	Quantity {
	   { Name a; Type Local; NameOfSpace VecPotA; }
	   { Name js; Type Local; NameOfSpace j_region; }
	}
	Equation {
	   Integral { [ nu[] * Dof{d a} , {d a} ]; 
		In Vol_tot; Jacobian Vol; Integration Int; }
	   Integral { [ -Dof{js} , {a} ]; 
		In Vol_source; Jacobian Vol; Integration Int; }	
	}
   }
}



Resolution{ // what to do with the weak formulation
  { Name MagSta_a;
    System {
      { Name Sys_Mag; NameOfFormulation AMagneto; }
    }
    Operation {
      Generate[Sys_Mag]; Solve[Sys_Mag]; SaveSolution[Sys_Mag];
    }
  }
}



PostProcessing{  // Obtain physical quantities
  { Name MagSta; NameOfFormulation AMagneto;
    Quantity {
      { Name a;
        Value {
          Term { [ {a} ]; In Dom_VecPotA; Jacobian Vol; }
        }
      }
      { Name az;
        Value {
          Term { [ CompZ[{a}] ]; In Dom_VecPotA; Jacobian Vol; }
        }
      }
      { Name b;
        Value {
          Term { [ {d a} ]; In Dom_VecPotA; Jacobian Vol; }
        }
      }
      { Name h;
        Value {
          Term { [ nu[] * {d a} ]; In Dom_VecPotA; Jacobian Vol; }
        }
      }
      { Name js;
        Value {
          Term { [ {js} ]; In Dom_VecPotA; Jacobian Vol; }
        }
      }
    }
  }
}



PostOperation{ // What to do with these quantities
  { Name Map_a; NameOfPostProcessing MagSta;
    Operation {
      Echo[ Str["l=PostProcessing.NbViews-1;",
		"View[l].IntervalsType = 1;",
		"View[l].NbIso = 40;"],
	    File "tmp.geo", LastTimeStepOnly] ;
      Print[ a, OnElementsOf Dom_VecPotA, File "T1_results/a.pos" ];
      Print[ js, OnElementsOf Dom_VecPotA, File "T1_results/js.pos" ];
      Print[ az, OnElementsOf Dom_VecPotA, File "T1_results/az.pos" ];
      Print[ b, OnElementsOf Dom_VecPotA, File "T1_results/b.pos" ];
      Print[ b, OnPlane{{0,0,0}{0.06,0,0}{0,0.06,0}} {75,16}, 
		File "T0_results/bsection.pos"];
//      Print[ h, OnElementsOf Dom_VecPotA, File "T1_results/h.pos" ];
    }
  }
}










