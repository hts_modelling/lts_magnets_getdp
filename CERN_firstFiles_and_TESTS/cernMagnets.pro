/* ********************************************************************
 *
 * CERN Test 0 Magnet
 *	Compute static solution of Test 0 magnet
 * 	
 * 	Author: Jorge Miaja Hernandez
 *
 ******************************************************************** */
 

// FLAGS AND CONSTANTS
DefineConstant[
//----------------------------- FLAGS ----------------------------------
	// Determines whether statics or dynamics
	Flag_magnetName = {1, Choices{0="T0 Magnet", 1="T1 Magnet"}, 
		Name "Flags/Choose test magnet"},	
	Flag_CteCurrent = {1, Choices{1="Magnetostatic: Const current",
		0="Magnetodynamic: Ramp up excitation"}, Name "Flags/Type of analysis"}, 
	// Determines if there are non linearities
	Flag_IronYokeNL	= {1, Choices{0,1}, Name "Flags/Non linear iron yoke"},
	// Non linearity parameters
	Flag_NewtonMethod_NL = {1, Choices{0="Picard Banach method", 
		1="Newton Raphson method"}, Name "Flags/Non linear method",
		Visible Flag_IronYokeNL},
	Flag_ifcc = {1, Choices{0, 1}, Name "Flags/IFCC", Visible !Flag_CteCurrent},
	Flag_PostOp = {0, Choices{0, 1}, Name "Flags/Show results every N timesteps"},
//----------------------------------------------------------------------		
	everyNsteps = {10, Min 1, Max 50, Step 1, Name "N", Visible(Flag_PostOp)},
	stopWhen = 1e-3,
	relaxFactor = 1,
	NmaxIterations = 30,
	// Time analysis - manetodynamic sparameters
	timeO = 0,
	timeMax = 1, // ramp up function from 0 to 1 second
	deltaTime = 20e-3, // 20ms time step - 50 iterations
	currentInit = 0,
	currentMax = 20*kAmp,
	effCoilLength = 9.2
];
If( !Flag_magnetName )
	Include "cernT0_common.pro";
	folder = "T0_results/";
Else
	Include "cernT1_common.pro";
	folder = "T1_results/";
EndIf

filename = "";
filename2 = "";
If(Flag_CteCurrent)
	filename = "MagSta";
ElseIf(Flag_ifcc)
	filename = "ifcc";
Else	
	filename = "noIfcc";
EndIf


Group{ // regions used in the formulation of the problem
  // Physical regions to be referenced in functions 
  Air = Region[ Air ];
  Coils = Region[ coils_surface ];
  Shield = Region[ ironYoke ];
  Surface_ht0 = Region[ xaxis_ht0Neu ];
  Surface_bn0 = Region[ yaxis_bn0Dir ];

  // Abstract regions to be referenced in objects
//  Vol_tot = Region[ {Air, Coils, Shield} ]; // goes later
  Vol_source = Region[ Coils ];
  Vol_yoke = Region [ Shield ];
//  Sur_Dir = Region[ Surface_bn0 ]; //goes later
  Sur_Neu = Region[ {} ];

  // With infinity mapping region
  If(Flag_infinityExpansion)
	Surface_Inf = Region[ surfInf ];
	Sur_Dir = Region[ {Surface_Inf, Surface_bn0} ];
	AirInf = Region [ AirInf ];
	Vol_tot = Region[ {Air, AirInf, Coils, Shield} ];
	Vol_inf = Region[ AirInf ];
  Else
	Vol_tot = Region[ {Air, Coils, Shield} ];
	Sur_Dir = Region[ Surface_bn0 ];
  EndIf
}




If(Flag_IronYokeNL)
	Include "ironBHcurve.pro";
EndIf



Function{ // Material laws. Physical (and not abstract) regions as argument
  mu0 = 4.e-7 * Pi;
  
  If(Flag_IronYokeNL)
	// Found in CERN's note crosscheck nu[] = Interp[$1]{List[x, f(x)]};
	nu [ Shield ]  = nuIronYoke[$1];
	newtonJacobi [ Shield ] = dhdbForNewtonJacobi[$1];
  Else
	nu [ Shield ] = 1 / (1.5 * mu0);
  EndIf
  
  nu [ Region[{Air, Coils}] ]  = 1. / mu0;
 
  If(Flag_infinityExpansion)
	nu [ AirInf ]  = 1. / mu0;
  EndIf

  
//  areaUsed[] = SurfaceArea[]{Region[ Coils ]};
//  rndNum = Rand[5];
//  Printf( "rand = %e", rndNum);
  If(Flag_CteCurrent)
	Current = 19.72*kAmp; // Found in CERN's note crosscheck
	jsz[ Coils ] = -numberCoils * Current / SurfaceArea[];
  Else
    // Time function specified in constraint 
	jsz = -numberCoils * currentMax / timeMax;
  EndIf  
  
    tau [ Coils ] = 1e-3;
}




Constraint{ // Boundary conditions
  { Name Dir_a;
    Case { // Dirichlet conditions - Essential
      { Region Sur_Dir; Value 0; }
    }
  }
  { Name Source_jDensZ; Type Assign;
    If(Flag_CteCurrent)
		Case {{ Region Vol_source; Value jsz[]; }}
	Else
		Case {{ Region Vol_source; Value jsz / SurfaceArea[];
			TimeFunction $Time; }}
	EndIf
  }
}



Group{ // Function space domain
	Dom_VecPotA = Region[ {Vol_tot, Sur_Neu} ];
}



FunctionSpace{ // where the solution is defined - FE expansion!!
   { Name VecPotA; Type Form1P;
       BasisFunction{ { // a(x,y) = sum_k aCoef_k sBF_k(x,y)
            Name sBF; NameOfCoef aCoef; Function BF_PerpendicularEdge;
            Support Dom_VecPotA; Entity NodesOf[All];
       } }
       Constraint{ { 
            NameOfCoef aCoef; EntityType NodesOf; NameOfConstraint Dir_a;
       } }
   }

   { Name j_region; Type Vector; 
       BasisFunction{ { // js(x,y) = sum_k jsCoef_k jBF_(x,y) 
            Name jBF; NameOfCoef jsCoef; Function BF_RegionZ; 
	    Support Vol_source; Entity Vol_source; 
       } } 
       Constraint { { 
			NameOfCoef jsCoef; EntityType Region; 
			NameOfConstraint Source_jDensZ; 
       } } 
   } 
} 
 
 
 
Jacobian{ // mapping between mesh and reference elements
   { Name Vol;
	Case{ 
	    If(Flag_infinityExpansion)	    
			{ Region All; Jacobian Vol; }
			{ Region Vol_inf; Jacobian VolSphShell {rInf, rInfExt}; }
	    Else
			{ Region All; Jacobian Vol; }
	    EndIf
	} 
   }

   { Name Sur; // if non-homogeneous Neumann boundary conditions
	Case{ {
		Region All; Jacobian Sur;
	} }
   }
}



Integration{ // Tell GetDP how to compute integrations
  { Name Int ;
    Case { { Type Gauss ;
             Case { { GeoElement Point       ; NumberOfPoints  1  ; }
					{ GeoElement Line        ; NumberOfPoints  4  ; }
					{ GeoElement Triangle    ; NumberOfPoints  16 ; }
                    { GeoElement Quadrangle  ; NumberOfPoints  4  ; }
			      }
           }
         }
  }
}



Formulation{ // FEM formulation of the problem
   { Name AMagneto; Type FemEquation; 
	Quantity {
	   { Name a; Type Local; NameOfSpace VecPotA; }
	   { Name js; Type Local; NameOfSpace j_region; }
	}
	Equation {
		Integral { [ nu[{d a}] * Dof{d a} , {d a} ]; 
		 In Vol_tot; Jacobian Vol; Integration Int; }
		// If the non linear method to be used is Newton-Raphson
		// Not only does the newton method has to be chosen, 
		// but also non linearity has to be active
		If(Flag_NewtonMethod_NL && Flag_IronYokeNL)
			Integral { JacNL[ newtonJacobi[{d a}] * Dof{d a} , {d a} ]; 
			 In Vol_yoke; Jacobian Vol; Integration Int; }
		EndIf
		If( Flag_ifcc ) // eddy currents formulation
			Integral { DtDof[ nu[] * tau[] * Dof{d a}, {d a}]; 
			 In Vol_source; Jacobian Vol; Integration Int;}
		EndIf 
	    Integral { [ -Dof{js} , {a} ]; // Dof with constraint -> more efficient
		 In Vol_source; Jacobian Vol; Integration Int; }	
	}
   }
}



Resolution{ // what to do with the weak formulation
  { Name MagSta_a;
    System {
		{ Name Sys_Mag; NameOfFormulation AMagneto; }
    }
    Operation {
		If(Flag_CteCurrent)
			// If the non linearity is included
			If(Flag_IronYokeNL) 
				// Iterative method to solve non linear region
				// Method type, Newton or Picard specified in Formulation
				IterativeLoop[NmaxIterations, stopWhen, relaxFactor]{
					// Generate and solve until convergence
					GenerateJac[Sys_Mag]; SolveJac[Sys_Mag]; }
			Else
				// Just one iteration if linear characteristics
				Generate[Sys_Mag]; Solve[Sys_Mag];
			EndIf
			// After obtaining the final solution, save the result
			SaveSolution[Sys_Mag];
		Else
			InitSolution[Sys_Mag]; // Initial Conditions
			If(Flag_PostOp)
				PostOperation[Map_timeSim];
			EndIf
			// Time simulation
			TimeLoopTheta[timeO, timeMax, deltaTime, 1]{
			
				UpdateConstraint[Sys_Mag, Vol_source, Assign];
			
				// $TimeStep, $Time - system already knows
					
				// If the non linearity is included
				If(Flag_IronYokeNL) 
					// Iterative method to solve non linearity at each timestep
					IterativeLoop[NmaxIterations, stopWhen, relaxFactor]{
						// Generate and solve until convergence
						GenerateJac[Sys_Mag]; SolveJac[Sys_Mag]; }
				Else
					// Just one iteration if linear characteristics
					Generate[Sys_Mag]; Solve[Sys_Mag];
				EndIf
				
				// Save solution for each time step
				SaveSolution[Sys_Mag]; 
				If(Flag_PostOp)
					Test[ $TimeStep/everyNsteps == 0 ]{ PostOperation[Map_timeSim]; }
				EndIf
			}
		EndIf
		PostOperation[Flux];
    }
  }
  
}



PostProcessing{  // Obtain physical quantities
  { Name MagSta; NameOfFormulation AMagneto;
    Quantity {
      { Name a;
        Value {
          Term { [ {a} ]; In Dom_VecPotA; Jacobian Vol; }
        }
      }
      { Name az;
        Value {
          Term { [ CompZ[{a}] ]; In Dom_VecPotA; Jacobian Vol; }
        } 
      } 
      { Name b;
        Value {
          Term { [ {d a} ]; In Dom_VecPotA; Jacobian Vol; }
        }
      }
      { Name h;
        Value {
          Term { [ nu[] * {d a} ]; In Dom_VecPotA; Jacobian Vol; }
        }
      }
      { Name js;
        Value {
          Term { [ {js} ]; In Dom_VecPotA; Jacobian Vol; }
        }
      }
    }
  }
}


PostOperation{ // What to do with these quantities
  { Name Map_a; NameOfPostProcessing MagSta;
    Operation {
      Print[ a, OnElementsOf Dom_VecPotA, File StrCat[folder,"a.pos"] ];
      Print[ js, OnElementsOf Dom_VecPotA, File StrCat[folder,"js.pos"] ];
      Print[ az, OnElementsOf Dom_VecPotA, File StrCat[folder,"az.pos"] ];
      Print[ b, OnElementsOf Dom_VecPotA, File StrCat[folder,"b.pos"] ];
    }
  }
  
  { Name Map_timeSim; NameOfPostProcessing MagSta;
    Operation {
//      Print[ a, OnElementsOf Dom_VecPotA, File "T0_results/a.pos" ];
      Print[ js, OnElementsOf Dom_VecPotA, File "T0_results/js.pos" ];
//      Print[ az, OnElementsOf Dom_VecPotA, File "T0_results/az.pos" ];
//      Print[ b, OnElementsOf Dom_VecPotA, File "T0_results/b.pos" ];
    }
  }
}




PostProcessing{  // Differential flux
  { Name MagFlux; NameOfFormulation AMagneto;
    Quantity {
	  /* Get the flux linkage by integrating the vector potential A 
	  over the surface of the coils and dividing the result by its area.
	  This computes the average vector potential  */
	  { Name fluxLinkage; Value { Integral{ [ -4*effCoilLength*{a}/SurfaceArea[] ]; 
			In Vol_source; Jacobian Vol; Integration Int; } } }
/*	  { Name test; Value { Integral{ [ 1/SurfaceArea[] ]; 
			In Vol_source; Jacobian Vol; Integration Int; } } }  */
    }
  }
}


PostOperation{ 
	{ Name Flux; NameOfPostProcessing MagFlux;
		Operation {
			Print[ fluxLinkage[Vol_source], OnGlobal, Format Table,
				File StrCat[folder, "fluxTable", filename, ".txt"] ];
//			Print[ fluxLinkage[Vol_source], OnGlobal, File "T0_results/flux.txt" ];
//			Print[ test[Vol_source], OnGlobal,File "T0_results/test.txt" ];
		}
	}
}




// Other possible prints
//      Print[ b, OnPlane{{0,0,0}{0.06,0,0}{0,0.06,0}} {75,16}, 
//		File "T0_results/bsection.pos"];
//      Print[ b, OnLine{{0,yDist,0}{0.1,yDist,0}} {50}, File "T0_results/bcut.pos"];
//      Print[ b, OnLine{{0,height,0}{0.098,height,0}} {50}, File >> "T0_results/bcut.pos"];
//      Print[ b, OnLine{{0,2*height,0}{0.094,2*height,0}} {50}, File >> "T0_results/bcut.pos"];
//      Print[ b, OnLine{{0,3*height,0}{0.088,3*height,0}} {50}, File >> "T0_results/bcut.pos"];
//      Print[ h, OnElementsOf Dom_VecPotA, File "T0_results/h.pos" ];

/*		If( Flag_ifcc )
			Echo[ StrCat[ "tau=", tau ],
				"View[l].IntervalsType = 1;",
				"View[l].NbIso = 40;"],
				File StrCat[folder, "fluxTable", filename, ".txt"],
				LastTimeStepOnly];
		EndIf
*/











