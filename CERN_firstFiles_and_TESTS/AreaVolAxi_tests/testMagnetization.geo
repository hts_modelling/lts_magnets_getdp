/*********************************************************************
 *
 * CERN Testing eddy current calculation through magnetization process
 * 		of the superconductors
 * The model tests if the eddy current formulation for superconducting 
 * materials has been properly modelled. As superconductor materials try
 * to expel all magnetic fields from within, even if there is no magnetic 
 * field variation. Our eddy current formulation is tested by applying a 
 * constant magnetic field in all regions of space and checking if the 
 * magnetic field lines avoid entering the block in the middle of the 
 * our domain. 
 *
 * 
 *
 *	Block design in Gmsh
 *
 * 	Author: Jorge Miaja Hernandez
 *
 *********************************************************************/
Include "testMag_common.pro";
 
If (Flag_AxiSym == 0)
	// Outside region
	Point(1) = {0, 0, 0, lc0};
	Point(2) = {radius, 0, 0, lc0};
	Point(3) = {-radius, 0, 0, lc0};
	Circle(1) =  {2, 1, 3}; Circle(2) =  {3, 1, 2};
	Curve Loop(3) = {1,2}; // boundary

	// Inside block
	Point(4) = {-blockLength/2, -blockHeight/2, 0, lcB};
	Point(5) = {blockLength/2, -blockHeight/2, 0, lcB};
	Point(6) = {blockLength/2, blockHeight/2, 0, lcB};
	Point(7) = {-blockLength/2, blockHeight/2, 0, lcB};
	Line(4)={4,5}; Line(5)={5,6}; Line(6)={6,7}; Line(7)={4,7};
	Curve Loop(8) = {4,5,6,-7};

	Plane Surface(9) = {3, 8}; // air
	Plane Surface(10) = {8}; // block

	// Physical regions
	Physical Curve ("contour", 100) = {1,2};
	Physical Surface("Air", 110) = {9};
	Physical Surface("block", 111) = {10}; 
	
ElseIf (Flag_AxiSym == 1)

	// All points
	Point(1)={0,0,0,lc0}; Point(2)={blockLength/2,0,0,lcB};
	Point(3)={radius,0,0,lc0}; Point(4)={0,radius,0,lc0}; 
	Point(5) = {0, blockHeight/2, 0, lcB};
	Point(6) = {blockLength/2, blockHeight/2, 0, lcB};
	
	//Outside boundary
	Line(1)={1,2}; Line(2)={2,3}; Circle(3)={3,1,4}; Line(4)={4,5}; 
	Line(5)={5,1}; 
	//Inner Lines
	Line(6)={5,6}; Line(7)={6,2};
	
	Curve Loop(8) = {6,7,2,3,4}; // Air boundary
	Plane Surface(9) = {8}; // air
	Curve Loop(10) = {1,-7,-6,5}; // Block boundary
	Plane Surface(11) = {10}; // block
	
	// Physical Regions
	Physical Curve ("contour", 100) = {3};
	Physical Curve ("dir", 101) = {4,5};
	Physical Curve ("neu", 102) = {1,2};
	Physical Surface("Air", 110) = {9};
	Physical Surface("block", 111) = {11}; 
	
Else
	// No other options implemented
EndIf
	
	
	
	
	
	
	
	
	
	
	
	



