/*********************************************************************
 *
 * CERN Testing eddy current calculation through magnetization process
 * 		of the superconductors
 *
 * For testing simmetries and area calculation - needed to fix errors 
 * in CERN T0 and T1 results
 *
 * 	Author: Jorge Miaja Hernandez
 *
 *********************************************************************/
DefineConstant[
	Flag_AxiSym = {0, Choices{0="Whole system", 1="1/4 only"}, 
		Name "Flags/Choose Test Type"}
]; 

If(Flag_AxiSym)
	folder = "results/sym/";
Else
	folder = "results/whole/";
EndIf

// Variables  
cm = 1e-2;
radius = 1;
blockHeight = 15*cm;
blockLength = 45*cm;
lc0 = blockLength / 5;
lcB = blockHeight / 2;
