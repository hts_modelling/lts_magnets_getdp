Function{ 
	
	// Magnetic field and flux obtained from the note crosscheck 
	// thank you CERN

	magneticField() = { // H(A/m)
		0, 7.941831506, 15.88366301, 23.82549452, 31.76732602, 47.53162283, 
		63.36753936, 79.21937139,158.8127569, 317.649387, 475.1809466,
		633.6276471, 792.1061786, 1192.094374, 1589.758907, 1987.081257,
		2386.281635, 2782.290957, 3179.493941, 3577.039107, 3971.377302, 
		4765.210312, 5559.099026, 6354.069994, 7149.486595, 7944.083548,
		10590, 13160, 21170, 33760, 66000, 120960, 169600, 212170, 283130,
		339890, 425040, 566950, 850760, 1702300, 2128000, 2837700, 3405100,
		4256700};
	
	magneticFlux() = { // B(T)
		0, 0.00712, 0.0167, 0.02925, 0.04522, 0.08891, 0.15434, 0.24066, 0.76507,
		1.29683, 1.45217, 1.52043, 1.55841, 1.60954, 1.63892, 1.66055,1.67826,
		1.69427, 1.70828, 1.72175, 1.73414, 1.75706, 1.77805, 1.79764, 1.81567,
		1.8333, 1.85, 1.9, 2, 2.1, 2.2, 2.28, 2.3443, 2.3996, 2.4905, 2.5627,
		2.6706, 2.8498, 3.2074, 4.2782, 4.8134, 5.7052, 6.4186, 7.4887};
		
		
	// x = squared norm of magnetic flux: ||B(T)||^2
	// f(x) = nu: H/B = nu(m/H)
	// nu obtained from values of the table: H/B
	iron_nu() = magneticField() / magneticFlux(); 
	iron_nu(0) = iron_nu(1); // Avoid 0/0
	// Squared value of the magnitudes in the table 
	magneticFlux2() = magneticFlux()^2;
	// list of x and f(x); x = B squared and f(x) = nu
	ironBH() = ListAlt[magneticFlux2(), iron_nu()];
	
	// Interpolation - [x, f(x)]
	nuIronYoke[] = InterpolationLinear[SquNorm[$1]]{ ironBH() };
	// derivative of the function, jacobian needed in a Newton-Raphson method
	dnuIronYoke[] = dInterpolationLinear[SquNorm[$1]]{ ironBH() };	 
	// Calculation of jacobian
	dhdbForNewtonJacobi[] = 2*dnuIronYoke[$1] * SquDyadicProduct[$1] ;
}