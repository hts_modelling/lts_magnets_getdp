/* ********************************************************************
 *
 * CERN Test 0 Magnet
 *	Compute static solution of Test 0 magnet
 * 	
 * 	Author: Jorge Miaja Hernandez
 *
 ******************************************************************** */
Include "cernT0_common.pro";

// FLAGS AND CONSTANTS
DefineConstant[
//----------------------------- FLAGS ----------------------------------
	// Determines whether statics or dynamics	
	Flag_CteCurrent = {1, Choices{1="Magnetostatic: Const current",
		0="Magnetodynamic: Ramp up excitation"}, Name "Flags/1Type of analysis"}, 
	// Determines if there are non linearities
	Flag_IronYokeNL	= {1, Choices{0,1}, Name "Flags/Non linear iron yoke"},
	// Non linearity parameters
	Flag_NewtonMethod_NL = {1, Choices{0="Picard Banach method", 
		1="Newton Raphson method"}, Name "Flags/Non linear method",
		Visible Flag_IronYokeNL},
	Flag_ifcc = {1, Choices{0, 1}, Name "Flags/IFCC", Visible !Flag_CteCurrent},
	Flag_PostOp = {0, Choices{0, 1}, Name "Flags/Show results every N timesteps"},
	Flag_ThermalForm = {0, Choices{0, 1}, Name "Flags/Thermal/1Thermal analysis" },
	// Flags for thermal materials
	Flag_Insulation = {0, Choices{0="None", 1="Glass Fiber"}, 
		Name "Flags/Thermal/3Insulation material", Visible (Flag_ThermalForm==1)},
	Flag_filling = {0, Choices{0="None", 1="Glass Fiber"}, 
		Name "Flags/Thermal/2Void filling material", Visible (Flag_ThermalForm==1)},
	Flag_quench = {0, Choices{0, 1}, 
		Name "Flags/Thermal/4Forced quench", Visible (Flag_ThermalForm==1)},
//----------------------------------------------------------------------		
	everyNsteps = {10, Min 1, Max 50, Step 1, Name "N", Visible(Flag_PostOp)},
	stopWhen = 1e-3,
	relaxFactor = 1,
	NmaxIterations = 30,
	// Time analysis - manetodynamic sparameters
	timeO = 0,
	timeMax = 1e-3, // ramp up function from 0 to 1 second
	deltaTime = 20e-6, // 20ms time step - 50 iterations
	currentInit = 0,
	currentMax = DefineNumber[20000, Name "Model Parameters/Max. Current"],
	effCoilLength = 9.2,
	
	// Thermal analysis variables
	initTemp = 1.9
];


filename = "";
filename2 = "";
If(Flag_CteCurrent)
	filename = "MagSta";
ElseIf(Flag_ifcc)
	filename = "ifcc";
Else	
	filename = "noIfcc";
EndIf

If (Flag_ThermalForm)
	filename = StrCat[filename, "_thermal"];
EndIf


Group{ // regions used in the formulation of the problem
  // Physical regions to be referenced in functions 
  Air = Region[ Air ];
  Coils = Region[ coils_surface ];
  FirstCoil = Region[ firstCoil ];
  Shield = Region[ ironYoke ];
  Surface_ht0 = Region[ xaxis_ht0Neu ];
  Surface_bn0 = Region[ yaxis_bn0Dir ];

  // Abstract regions to be referenced in objects
//  Vol_tot = Region[ {Air, Coils, Shield} ]; // goes later

// COIL DEFINITION
  Vol_coil = Region [ FirstCoil ];
  Vol_source = Region[ {Coils, FirstCoil} ];
//  Vol_source = Region[ Coils ];
  
  
  
  Vol_yoke = Region [ Shield ];
//  Sur_Dir = Region[ Surface_bn0 ]; //goes later
  Sur_Neu = Region[ {} ];
  Sur_Neu_T = Region[ {} ];

  // With infinity mapping region
  If(Flag_infinityExpansion)
	Surface_Inf = Region[ surfInf ];
	Sur_Dir = Region[ {Surface_Inf, Surface_bn0} ];
	AirInf = Region [ AirInf ];
//	Vol_tot = Region[ {Air, AirInf, Coils, Shield} ];
	Vol_tot = Region[ {Air, AirInf, Coils, FirstCoil, Shield} ];
	Vol_inf = Region[ AirInf ];
  Else
//	Vol_tot = Region[ {Air, Coils, Shield} ];
	Vol_tot = Region[ {Air, Coils, FirstCoil, Shield} ];
	Sur_Dir = Region[ Surface_bn0 ];
  EndIf
}




If(Flag_IronYokeNL)
	Include "ironBHcurve.pro";
EndIf

Include "thermalParam.pro";

Function{ // Material laws. Physical (and not abstract) regions as argument
  mu0 = 4.e-7 * Pi;
  
  If(Flag_IronYokeNL)
	// Found in CERN's note crosscheck nu[] = Interp[$1]{List[x, f(x)]};
	nu [ Shield ]  = nuIronYoke[$1];
	newtonJacobi [ Shield ] = dhdbForNewtonJacobi[$1];
  Else
	nu [ Shield ] = 1 / mu0;
  EndIf
  
//  nu [ Region[{Air, Coils}] ]  = 1. / mu0;
  nu [ Region[{Air, Coils, FirstCoil}] ]  = 1. / mu0;
 
  If(Flag_infinityExpansion)
	nu [ AirInf ]  = 1. / mu0;
  EndIf

  
//  areaUsed[] = SurfaceArea[]{coils_surface};
//  rndNum = Rand[5];
//  Printf( "rand = %e", rndNum);
  If(Flag_CteCurrent)
//	Current = 20*kAmp; // 19.72*kAmp; // Found in CERN's note crosscheck
	Current = currentMax;
//	jsz[ Coils ] = -numberCoils * Current / SurfaceArea[];
	jsz[ Region[{Coils, FirstCoil}] ] = -numberCoils * Current / SurfaceArea[];
  Else
    // Time function specified in constraint 
	jsz = -numberCoils * currentMax / timeMax;
  EndIf  
  
//    tau [ Coils ] = 1e-3;
	If( Flag_ifcc ) // eddy currents formulation
		tau [ Region[{Coils, FirstCoil}] ] = 1e-3;
	Else
		tau [] = 0;
	EndIf
	
	kappa = A_strands / A_coil;
	// thermal characteristics
	rho[ Region[{Coils, FirstCoil}] ] = rhoEff; // constant for now... 
	Cp[ Region[{Coils, FirstCoil}] ] = CpEff;
	sigmaTher[ Region[{Coils, FirstCoil}] ] = thCondEff;
}




Constraint{ // Boundary conditions
  { Name Dir_a;
    Case { // Dirichlet conditions - Essential
      { Region Sur_Dir; Value 0; }
    }
  }
  { Name Source_jDensZ; Type Assign;
    If(Flag_CteCurrent)
		Case {{ Region Vol_source; Value jsz[]; }}
	Else
		Case {{ Region Vol_source; Value jsz / SurfaceArea[];
			TimeFunction $Time; }}
	EndIf
  }
  { Name initTemperature; Type Init;
    Case { // Initial temperature for thermal analysis
      { Region Vol_source; Value initTemp; }
    }
  }
}



Group{ // Function space domain
	Dom_VecPotA = Region[ {Vol_tot, Sur_Neu} ];
	Dom_thermalT = Region[ {Vol_source, Sur_Neu_T} ];
}



FunctionSpace{ // where the solution is defined - FE expansion!!
   { Name VecPotA; Type Form1P;
       BasisFunction{ { // a(x,y) = sum_k aCoef_k sBF_k(x,y)
            Name sBF; NameOfCoef aCoef; Function BF_PerpendicularEdge;
            Support Dom_VecPotA; Entity NodesOf[All];
       } }
       Constraint{ { 
            NameOfCoef aCoef; EntityType NodesOf; NameOfConstraint Dir_a;
       } }
   }

   { Name j_region; Type Vector; 
       BasisFunction{ { // js(x,y) = sum_k jsCoef_k jBF_(x,y) 
            Name jBF; NameOfCoef jsCoef; Function BF_RegionZ; 
			Support Vol_source; Entity Vol_source; 
       } } 
       Constraint { { 
			NameOfCoef jsCoef; EntityType Region; 
			NameOfConstraint Source_jDensZ; 
       } } 
   } 
   
   { Name NodalT; Type Form0;
       BasisFunction{ { // T(x,y) = sum_k tCoef_k tBF_k(x,y)
            Name tBF; NameOfCoef tCoef; Function BF_Node;
            Support Dom_thermalT; Entity NodesOf[All];
       } }
       Constraint{ { 
            NameOfCoef tCoef; EntityType NodesOf; 
			NameOfConstraint initTemperature;
       } }
   }
} 
 
 
 
Jacobian{ // mapping between mesh and reference elements
   { Name Vol;
	Case{ 
	    If(Flag_infinityExpansion)	    
			{ Region All; Jacobian Vol; }
			{ Region Vol_inf; Jacobian VolSphShell {rInf, rInfExt}; }
	    Else
			{ Region All; Jacobian Vol; }
	    EndIf
	} 
   }

   { Name Sur; // if non-homogeneous Neumann boundary conditions
	Case{ {
		Region All; Jacobian Sur;
	} }
   }
}



Integration{ // Tell GetDP how to compute integrations
  { Name Int ;
    Case { { Type Gauss ;
             Case { { GeoElement Point       ; NumberOfPoints  1  ; }
					{ GeoElement Line        ; NumberOfPoints  4  ; }
					{ GeoElement Triangle    ; NumberOfPoints  16 ; }
                    { GeoElement Quadrangle  ; NumberOfPoints  4  ; }
			      }
           }
         }
  }
}



Formulation{ // FEM formulation of the problem
   { Name AMagneto; Type FemEquation; 
	Quantity {
	   { Name a; Type Local; NameOfSpace VecPotA; }
	   { Name js; Type Local; NameOfSpace j_region; }
	}
	Equation {
		Integral { [ nu[{d a}] * Dof{d a} , {d a} ]; 
		 In Vol_tot; Jacobian Vol; Integration Int; }
		// If the non linear method to be used is Newton-Raphson
		// Not only does the newton method has to be chosen, 
		// but also non linearity has to be active
		If(Flag_NewtonMethod_NL && Flag_IronYokeNL)
			Integral { JacNL[ newtonJacobi[{d a}] * Dof{d a} , {d a} ]; 
			 In Vol_yoke; Jacobian Vol; Integration Int; }
		EndIf
		Integral { DtDof[ nu[] * tau[] * Dof{d a}, {d a}]; 
		 In Vol_source; Jacobian Vol; Integration Int;}
	    Integral { [ -Dof{js} , {a} ]; // Dof with constraint -> more efficient
		 In Vol_source; Jacobian Vol; Integration Int; }	
	}
   }
   
   { Name Thermal; Type FemEquation;
    Quantity {
		{ Name T; Type Local; NameOfSpace NodalT; }
		{ Name a; Type Local; NameOfSpace VecPotA; }
	    { Name js; Type Local; NameOfSpace j_region; }
	} 
	Equation {
		Integral { DtDof[ Cp[] * Dof{T}, {T} ]; 
		  In Vol_source; Jacobian Vol; Integration Int; }
		Integral { [ sigmaTher[] * Dof{d T}, {d T} ]; 
		  In Vol_source; Jacobian Vol; Integration Int; }
/*		Integral { [ -kappa * nu[] * tau[] * SquNorm[ Dt[{d a}] ], {T} ]; 
		  In Vol_source; Jacobian Vol; Integration Int; }  */
		Integral { [ -Flag_quench * rho[] * SquNorm[ {js} ], {T} ]; 
		  In Vol_source; Jacobian Vol; Integration Int; }
	}
   }
}



Resolution{ // what to do with the weak formulation
  { Name MagSta_a;
    System {
		If(Flag_ThermalForm)
			{ Name Sys_Mag; NameOfFormulation AMagneto; }
			{ Name Sys_Ther; NameOfFormulation Thermal; }
		Else
			{ Name Sys_Mag; NameOfFormulation AMagneto; }
		EndIf
    }
    Operation {
		If(Flag_CteCurrent)
			// If the non linearity is included
			If(Flag_IronYokeNL) 
				// Iterative method to solve non linear region
				// Method type, Newton or Picard specified in Formulation
				IterativeLoop[NmaxIterations, stopWhen, relaxFactor]{
					// Generate and solve until convergence
					GenerateJac[Sys_Mag]; SolveJac[Sys_Mag]; }
			Else
				// Just one iteration if linear characteristics
				Generate[Sys_Mag]; Solve[Sys_Mag];
			EndIf
			// After obtaining the final solution, save the result
			SaveSolution[Sys_Mag];
			If(Flag_ThermalForm)
				Generate[Sys_Ther]; Solve[Sys_Ther]; 
				SaveSolution[Sys_Ther];
			EndIf
		Else
			InitSolution[Sys_Mag]; // Initial Conditions
			If(Flag_ThermalForm)
				InitSolution[Sys_Ther];
			EndIf
			If(Flag_PostOp)
				PostOperation[Map_timeSim];
			EndIf
			// Time simulation
			TimeLoopTheta[timeO, timeMax, deltaTime, 1]{
			
//				UpdateConstraint[Sys_Mag, Vol_source, Assign]; // not necessary
			
				// $TimeStep, $Time - system already knows
					
				// If the non linearity is included
				If(Flag_IronYokeNL) 
					// Iterative method to solve non linearity at each timestep
					IterativeLoop[NmaxIterations, stopWhen, relaxFactor]{
						// Generate and solve until convergence
						GenerateJac[Sys_Mag]; SolveJac[Sys_Mag]; }
				Else
					// Just one iteration if linear characteristics
					Generate[Sys_Mag]; Solve[Sys_Mag];
				EndIf
				
				// Save solution for each time step
				SaveSolution[Sys_Mag]; 
				If(Flag_ThermalForm)
					Generate[Sys_Ther]; Solve[Sys_Ther]; SaveSolution[Sys_Ther];
				EndIf
				If(Flag_PostOp)
					Test[ $TimeStep/everyNsteps == 0 ]{ PostOperation[Map_timeSim]; }
				EndIf
			}
		EndIf
		PostOperation[Flux];
    }
  }
}



PostProcessing{  // Obtain physical quantities
  { Name MagSta; NameOfFormulation Thermal;
    Quantity {
      { Name a;
        Value {
          Term { [ {a} ]; In Dom_VecPotA; Jacobian Vol; }
        }
      }
      { Name az;
        Value {
          Term { [ CompZ[{a}] ]; In Dom_VecPotA; Jacobian Vol; }
        } 
      } 
      { Name b;
        Value {
          Term { [ {d a} ]; In Dom_VecPotA; Jacobian Vol; }
        }
      }
      { Name h;
        Value {
          Term { [ nu[] * {d a} ]; In Dom_VecPotA; Jacobian Vol; }
        }
      }
      { Name js;
        Value {
          Term { [ {js} ]; In Dom_VecPotA; Jacobian Vol; }
        }
      }
	  { Name js1;
        Value {
          Term { [ {js} ]; In Vol_coil; Jacobian Vol; }
        }
      }
    }
  }
}


PostOperation{ // What to do with these quantities
  { Name Map_a; NameOfPostProcessing MagSta;
    Operation {
//      Print[ a, OnElementsOf Dom_VecPotA, File "T0_results/a.pos" ];
      Print[ js, OnElementsOf Dom_VecPotA, File "T0_results/js.pos" ];
      Print[ js1, OnElementsOf Vol_coil, File "T0_results/js1.pos" ];
//      Print[ az, OnElementsOf Dom_VecPotA, File "T0_results/az.pos" ];
      Print[ b, OnElementsOf Dom_VecPotA, File "T0_results/b.pos" ];    
//	  Print[ b, OnElementsOf FirstCoil, File "T0_results/bsource.pos" ];
//	  Print[ b, OnElementsOf Air, File "T0_results/bair.pos" ];
    }
  }
  
  { Name Map_timeSim; NameOfPostProcessing MagSta;
    Operation {
//      Print[ a, OnElementsOf Dom_VecPotA, File "T0_results/a.pos" ];
      Print[ js, OnElementsOf Dom_VecPotA, File "T0_results/js.pos" ];
//      Print[ az, OnElementsOf Dom_VecPotA, File "T0_results/az.pos" ];
//      Print[ b, OnElementsOf Dom_VecPotA, File "T0_results/b.pos" ];
    }
  }
}







PostProcessing{  // Differential flux
  { Name MagFlux; NameOfFormulation Thermal;
    Quantity {
	  /* Get the flux linkage by integrating the vector potential A 
	  over the surface of the coils and dividing the result by its area.
	  This computes the average vector potential  */
	  { Name fluxLinkage; Value { Integral{ Type Global; [ -4*effCoilLength*{a}/SurfaceArea[] ]; 
			In Vol_source; Jacobian Vol; Integration Int; } } }
	  { Name fluxLinkageN; Value { Integral{ Type Global; [ 4*16*Norm[{a}]/SurfaceArea[] ]; 
			In Vol_source; Jacobian Vol; Integration Int; } } }
	  { Name fluxLinkageZ; Value { Integral{ Type Global; [ -4*effCoilLength*CompZ[{a}]/SurfaceArea[] ]; 
			In Vol_source; Jacobian Vol; Integration Int; } } }		
			
			// Only first coil
	  { Name bHT0; Value { Integral{ Type Global; [ Norm[{d a}] / SurfaceArea[] ]; 
			In Vol_coil; Jacobian Vol; Integration Int; } } }
	  { Name areaHT0; Value { Integral{ Type Global; [ 1 ]; 
			In Vol_coil; Jacobian Vol; Integration Int; } } } 
      { Name areaHT0divA; Value { Integral{ Type Global; [ 1/SurfaceArea[] ]; 
			In Vol_coil; Jacobian Vol; Integration Int; } } } 
      { Name areaHT0xA; Value { Integral{ Type Global; [ SurfaceArea[] ]; 
			In Vol_coil; Jacobian Vol; Integration Int; } } } 	

			// All 16 coils
	  { Name areaC; Value { Integral{ Type Global; [ 1 ]; 
			In Vol_source; Jacobian Vol; Integration Int; } } } 
      { Name areaCdivA; Value { Integral{ Type Global; [ 1/SurfaceArea[] ]; 
			In Vol_source; Jacobian Vol; Integration Int; } } } 
      { Name areaCxA; Value { Integral{ Type Global; [ SurfaceArea[] ]; 
			In Vol_source; Jacobian Vol; Integration Int; } } }			
			
			
			
			
	  { Name MifccXY; 
		Value { 
			Integral{ Type Global; [ nu[]*tau[]*Dt[{d a}] / SurfaceArea[] ]; 
				In Vol_coil; Jacobian Vol; Integration Int; }
		} 
	  }
	  { Name MifccXY_handDer; 
		Value { 
			Integral{ Type Global; [ nu[]*tau[]*{d a} / SurfaceArea[] ]; 
				In Vol_coil; Jacobian Vol; Integration Int; }
		} 
	  }
	  { Name Qifcc; 
		Value { 
			Integral{ Type Global; [ nu[]*tau[] * Dt[{d a}]#0 * #0 ]; 
				In Vol_coil; Jacobian Vol; Integration Int; }
		} 
	  }
	  { Name Qifcc_B; 
		Value { 
			Integral{ Type Global; [ nu[]*tau[] * Dt[{d a}] ]; 
				In Vol_coil; Jacobian Vol; Integration Int; }
		} 
	  }
	  { Name Qifcc_N2; 
		Value { 
			Integral{ Type Global; [ nu[]*tau[] * SquNorm[Dt[{d a}]] ]; 
				In Vol_coil; Jacobian Vol; Integration Int; }
		} 
	  }
	  { Name Qifcc_tot; 
		Value { 
			Integral{ Type Global; [ nu[]*tau[] * Dt[{d a}]#1 * #1 ]; 
				In Vol_source; Jacobian Vol; Integration Int; }
		} 
	  }
	  
	  
	  
	If(Flag_ThermalForm)
	  { Name T;
        Value {
          Term { [ {T} ]; In Dom_thermalT; Jacobian Vol; }
        }
      }
	  { Name rho_HT0; 
		Value {
			Integral{ Type Global; [ rho[] / SurfaceArea[] ]; 
				In Vol_coil; Jacobian Vol; Integration Int; }
		}
	  }
	  { Name temp_HT0; 
		Value {
			Integral{ Type Global; [ {T} / SurfaceArea[] ]; 
				In Vol_coil; Jacobian Vol; Integration Int; }
		}
	  }
	  { Name Qjoule_HT0; 
		Value {
			Integral{ Type Global; [Flag_quench*rho[]*SquNorm[{js}]]; 
				In Vol_coil; Jacobian Vol; Integration Int; }
		}
	  }
	  { Name Qjoule_tot; 
		Value {
			Integral{ Type Global; [4*Flag_quench*rho[]*SquNorm[{js}]]; 
				In Vol_source; Jacobian Vol; Integration Int; }
		}
	  }
	EndIf  
	  
	  
    }
  }
}


PostOperation{ 
	{ Name Flux; NameOfPostProcessing MagFlux;
		Operation {
//			Print[ fluxLinkage[Vol_source], OnGlobal, 
//				File StrCat["T0_results/fluxTable", filename, filename2, ".txt"] ];
			Print[ fluxLinkageN[Vol_source], OnGlobal, 
				File StrCat["T0_results/fluxTableN", filename, filename2, ".txt"] ];
//			Print[ fluxLinkageZ[Vol_source], OnGlobal,
//				File StrCat["T0_results/fluxTableZ", filename, filename2, ".txt"] ];
//			Print[ fluxLinkage[Vol_source], OnGlobal, File "T0_results/flux.txt" ];

			Print[ bHT0[Vol_coil], OnGlobal, File "T0_results/bHT0.txt" ];
			Print[ areaHT0[Vol_coil], OnGlobal, File "T0_results/areaHT0.txt" ];
//			Print[ areaHT0divA[Vol_coil], OnGlobal, File "T0_results/areaHT0divA.txt" ];
//			Print[ areaHT0xA[Vol_coil], OnGlobal, File "T0_results/areaHT0xA.txt" ];
			
			Print[ areaC[Vol_source], OnGlobal, File "T0_results/areaC.txt" ];
//			Print[ areaCdivA[Vol_source], OnGlobal, File "T0_results/areaCdivA.txt" ];
//			Print[ areaCxA[Vol_source], OnGlobal, File "T0_results/areaCxA.txt" ];              



			Print[MifccXY[Vol_coil],OnGlobal,Format Table,
				File StrCat["T0_results/M_ifccXY", filename, filename2, ".txt"]];	
			Print[MifccXY_handDer[Vol_coil],OnGlobal,Format Table,
				File StrCat["T0_results/M_ifccXY_handDer", filename, filename2, ".txt"]];	
			Print[Qifcc[Vol_coil],OnGlobal,Format Table,
				File StrCat["T0_results/Q_ifcc_HT0", filename, filename2, ".txt"]];	
			Print[Qifcc_B[Vol_coil],OnGlobal,Format Table,
				File StrCat["T0_results/Q_ifcc_HT0_B", filename, filename2, ".txt"]];	
//			Print[Qifcc_N2[Vol_coil],OnGlobal,Format Table,
//				File StrCat["T0_results/Q_ifcc_HT0_N2", filename, filename2, ".txt"]];	
			Print[Qifcc_tot[Vol_source],OnGlobal,Format Table,
				File StrCat["T0_results/Q_ifcc_tot", filename, filename2, ".txt"]];
				
				
				
		If(Flag_ThermalForm)	
			Print[ T, OnElementsOf Dom_thermalT, File "T.pos" ] ;
			Print[ rho_HT0[Vol_coil], OnGlobal, Format Table,
				File "T0_results/rho_ht0.txt" ];
			Print[ temp_HT0[Vol_coil], OnGlobal, Format Table,
				File "T0_results/t_ht0.txt" ];
			Print[ Qjoule_HT0[Vol_coil], OnGlobal, Format Table,
				File "T0_results/Qjoule_HT0.txt" ];
			Print[ Qjoule_tot[Vol_source], OnGlobal, Format Table,
				File "T0_results/Qjoule_tot.txt" ];
		EndIf
				
				
		}
	}
}




// Other possible prints
//      Print[ b, OnPlane{{0,0,0}{0.06,0,0}{0,0.06,0}} {75,16}, 
//		File "T0_results/bsection.pos"];
//      Print[ b, OnLine{{0,yDist,0}{0.1,yDist,0}} {50}, File "T0_results/bcut.pos"];
//      Print[ b, OnLine{{0,height,0}{0.098,height,0}} {50}, File >> "T0_results/bcut.pos"];
//      Print[ b, OnLine{{0,2*height,0}{0.094,2*height,0}} {50}, File >> "T0_results/bcut.pos"];
//      Print[ b, OnLine{{0,3*height,0}{0.088,3*height,0}} {50}, File >> "T0_results/bcut.pos"];
//      Print[ h, OnElementsOf Dom_VecPotA, File "T0_results/h.pos" ];

/*		If( Flag_ifcc )
			Echo[ StrCat[ "tau=", tau ],
				"View[l].IntervalsType = 1;",
				"View[l].NbIso = 40;"],
				File StrCat[folder, "fluxTable", filename, ".txt"],
				LastTimeStepOnly];
		EndIf
*/




