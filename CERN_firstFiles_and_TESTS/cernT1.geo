/*********************************************************************
 *
 * CERN Test 1 Magnet
 *	Magnet design in Gmsh
 *
 * 	03.12.18 - redefinition of characteristic length of the mesh 
 * 		along the x-axis by means of two extra points
 *	04.12.18 - infinity region does not need to be implemented, as
		the results are exactly the same
 *********************************************************************/

Include "cernT1_common.geo";
Include "m1_rectangle_macro.geo";

// Create boundary points 

Point(1) = {0, 0, 0, lc0};
Point(2) = {xDist_1st[0], 0, 0, lc_coils};
Point(3) = {xDist_1st[0]+numberCoils[0]*(thickness+gapBet), 0, 0, lc_coils};
Point(4) = {shieldIntR, 0, 0, lc_shield};
Point(5) = {shieldExtR, 0, 0, lc_shieldExt};
Point(6) = {0, shieldIntR, 0, lc_shield};
Point(7) = {0, shieldExtR, 0, lc_shieldExt};

// Create boundary lines
Line(1) = {1,2}; Line(2) = {2,3}; Line(3) = {3,4}; 
Circle(4) = {4, 1, 6}; Line(5) = {6,1}; Line(6) = {4,5}; 
Circle(7) = {5, 1, 7}; Line(8) = {7,6};

Curve Loop(9) = {1,2,3,4,5}; Curve Loop(10) = {6,7,8,-4};

Compound Curve{1,2,3};

// Call defined function to create rectangular coils
Holes[] = {};
coilSurf[] = {};
For iii In {0:1} // begin for loop
	// assign nonvariable values
	m1_dx = thickness;
	m1_y = yDist[iii];
	m1_dy = height;
	m1_z = 0;
	m1_lc = lc_coils;

	For nRectangle In {0:numberCoils[iii]-1} // begin for loop
		// assign variable value
		m1_x = xDist_1st[iii] + (nRectangle)*(m1_dx+gapBet);

		Call m1_rectangle_macro ;
		// Printf("iteration %g", nRectangle);
		Holes[] += m1_ll1;
		coilSurf[] += m1_surf;
	EndFor // end for loop
EndFor // end for loop

S1 = news; Plane Surface(news) = {9, Holes[]};
S2 = news; Plane Surface(news) = {10};


// Neumann boundary - simmetry along x axis
Physical Curve("xaxis_ht0Neu", xaxis_ht0Neu) = {1,2,3,6}; 
// Diritchlet boundary - simmetry along y axis
Physical Curve("yaxis_bn0Dir", yaxis_bn0Dir) = {5,8}; 

Physical Surface("Air", Air) = {S1}; // Air
// Treat all coils as one single phys surface
Physical Surface("coils_surface", coils_surface) = {coilSurf[]};
Physical Surface("ironYoke", ironYoke) = {S2}; // Iron yolk 


