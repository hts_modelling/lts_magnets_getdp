/*********************************************************************
 *
 * CERN Test 0 Magnet
 *
 *	structure, dimensions and global variables
 *
 * 	Author: Jorge Miaja Hernandez
 *
 *********************************************************************/
// Used in .geo and .pro files
DefineConstant[
	Flag_infinityExpansion = {1, Choices{0, 1}, 
		Name "Model parameters/Infinity mapping region/Infinity region"},
	Flag_oldConfig = {0, Choices{0,1}, Name "Flags/Old mesh-inf setup"},
	Flag_reducedAirMesh = {1, Choices{0,1},
		Name "Model parameters/Infinity mapping region/Reduced mesh", 
		Visible (Flag_infinityExpansion==1)}
]; 

// Variables
mm = 1e-03;
cm = 1e-02;
kAmp = 1e3;


// MAGNET DIMENSIONS - in meters

// SHIELD
shieldIntR = 0.06;
shieldExtR = 0.1;

// COILS
numberCoils = 16; // number of coils == number of coils
height = 15e-3; // coil height
thickness = 15e-04; // coil thickness
xDist_1st = 15e-03; // distance along x-axis to origin 
yDist = 1e-03; // distance to x-axis
gapBet = 5e-4; // gap in between neighbouring coils

s = DefineNumber[5, Name "Model parameters/Global mesh size", 
	Help "Reduce for finer mesh, increase for coarser one"];
	
If (!Flag_oldConfig)
	dInf = DefineNumber[1, Name "Model parameters/Infinity region distance multiplier", 
		Help "The bigger this number is, the farther the infinity region mapping is"];
	rInf = 0.5*dInf;
	rInfExt = rInf * 1.05;
	lcInf = (rInfExt - rInf) / 5;
	lc0 = xDist_1st / 10 * s; // element length around the origin
	lc_coils = thickness / 10 * s; // element length around the coils
	lc_shield = (shieldIntR - xDist_1st - numberCoils*(thickness+gapBet) ) 
			/ 10 * s; // element length near shield
	lc_shieldExt = (shieldExtR - shieldIntR) / 10 * s;
Else
	// INFINITY RING
	rInf = 0.11;
	rInfExt = 0.115;
	lcInf = (rInfExt - rInf) / 5;

	// CHARACTERISTIC LENGTHS
	lc0 = xDist_1st / 5 * s; // element length around the origin
	lc_coils = thickness / 1 * s; // element length around the coils
	lc_shield = (shieldIntR - xDist_1st - numberCoils*(thickness+gapBet) ) 
			/ 5 * s; // element length near shield
	lc_shieldExt = (shieldExtR - shieldIntR) / 5 * s;
EndIf


If(Flag_reducedAirMesh)
	distP = 0.35;
	nPoints = DefineNumber[3, 
		Name "Model parameters/Infinity mapping region/Number of points", 
		Help "Number of points to be embedded in Air region to force reduced mesh",
		Visible (Flag_infinityExpansion==1 && Flag_reducedAirMesh==1)
	];
	lc_reduced = 0.03 * s;
	angleP = Pi/(2 * (nPoints+1));
EndIf


// PHYSICAL REGIONS

//curves
xaxis_ht0Neu = 1000;
yaxis_bn0Dir = 1001;
surfInf = 1002;

//surfaces
Air = 1100;
AirInf = 1101;
coils_surface = 1102;
firstCoil = 1111;
ironYoke = 1103;




