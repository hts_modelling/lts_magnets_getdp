/* ********************************************************************
 *
 * CERN Test 0 Magnet
 *	Compute static solution of Test 0 magnet
 * 	
 * 	Author: Jorge Miaja Hernandez
 *
 ******************************************************************** */
Include "cernT0_common.pro";

DefineConstant[
	stopWhen = 1e-3,
	relaxFactor = 1,
	NmaxIterations = 30
];


Group{ 
  Air = Region[ Air ];
  Coils = Region[ coils_surface ];
  Shield = Region[ ironYoke ];
  Surface_ht0 = Region[ xaxis_ht0Neu ];
  Surface_bn0 = Region[ yaxis_bn0Dir ];


  Vol_source = Region[ Coils ];
  Vol_yoke = Region [ Shield ];
 
  Sur_Neu = Region[ {} ];

  If(Flag_infinityExpansion)
	Surface_Inf = Region[ surfInf ];
	AirInf = Region [ AirInf ];
	Sur_Dir = Region[ {Surface_bn0, Surface_Inf} ];
	Vol_tot = Region[ {Air, AirInf, Coils, Shield} ];
	Vol_inf = Region[ AirInf ];
  Else
	Vol_tot = Region[ {Air, Coils, Shield} ];
	Sur_Dir = Region[ Surface_bn0 ];
  EndIf
}

Include "ironBHcurve.pro";

Function{ 
  mu0 = 4.e-7 * Pi;
  murIronYolk = 1; // Found in CERN's note crosscheck

  nu [ Region[{Air, Coils}] ]  = 1. / mu0;
  nu [ Shield ]  = nuIronYoke[$1];
  newtonJacobi [ Shield ] = dhdbForNewtonJacobi[$1];
  If(Flag_infinityExpansion)
	nu [ AirInf ]  = 1. / mu0;
  EndIf

  Current = 19.72*kAmp;
  jsz[ Coils ] = -numberCoils * Current / SurfaceArea[];
}



Constraint{ // Boundary conditions
  { Name Dir_a;
    Case {
      { Region Sur_Dir; Value 0; }
    }
  }
  { Name Source_jDensZ;
    Case {
      { Region Vol_source; Value jsz[]; }
    }
  }
}



Group{ // Function space domain
  Dom_VecPotA = Region[ {Vol_tot, Sur_Neu} ];
}



FunctionSpace{ 
   { Name VecPotA; Type Form1P;
       BasisFunction{ { // a(x,y) = sum_k aCoef_k sBF_k(x,y)
            Name sBF; NameOfCoef aCoef; Function BF_PerpendicularEdge;
            Support Dom_VecPotA; Entity NodesOf[All];
       } }
       Constraint{ { 
            NameOfCoef aCoef; EntityType NodesOf; NameOfConstraint Dir_a;
       } }
   }


   { Name j_region; Type Vector; 
       BasisFunction{ { // js(x,y) = sum_k jsCoef_k jBF_(x,y)
            Name jBF; NameOfCoef jsCoef; Function BF_RegionZ;
	    Support Vol_source; Entity Vol_source;
       } }
       Constraint { { 
	    NameOfCoef jsCoef; EntityType Region;
 	    NameOfConstraint Source_jDensZ;
       } }
   }

}



Jacobian{ 
   { Name Vol;
	Case{ 
	    If(Flag_infinityExpansion)	    
		{ Region All; Jacobian Vol; }
		{ Region Vol_inf; Jacobian VolSphShell {rInf, rInfExt}; }
	    Else
		{ Region All; Jacobian Vol; }
	    EndIf
	} 
   }

   { Name Sur; 
	Case{ {
	   Region All; Jacobian Sur;
	} }
   }
}



Integration{ 
  { Name Int ;
    Case { { Type Gauss ;
             Case { { GeoElement Point       ; NumberOfPoints  1  ; }
					{ GeoElement Line        ; NumberOfPoints  4  ; }
					{ GeoElement Triangle    ; NumberOfPoints  16 ; }
                    { GeoElement Quadrangle  ; NumberOfPoints  4  ; }
			      }
           }
         }
  }
}



Formulation{ 
   { Name AMagneto; Type FemEquation; 
	Quantity {
	   { Name a; Type Local; NameOfSpace VecPotA; }
	   { Name js; Type Local; NameOfSpace j_region; }
	}
	Equation {
	   Integral { [ nu[{d a}] * Dof{d a} , {d a} ]; 
		In Vol_tot; Jacobian Vol; Integration Int; }
	   Integral { JacNL[ newtonJacobi[{d a}] * Dof{d a} , {d a} ]; 
		In Vol_yoke; Jacobian Vol; Integration Int; }
	   Integral { [ -Dof{js} , {a} ]; 
		In Vol_source; Jacobian Vol; Integration Int; }	
	}
   }
}



Resolution{ 
  { Name MagSta_a;
    System {
      { Name Sys_Mag; NameOfFormulation AMagneto; }
    }
    Operation {
	  IterativeLoop[NmaxIterations, stopWhen, relaxFactor]{
		GenerateJac[Sys_Mag]; SolveJac[Sys_Mag]; }
      Generate[Sys_Mag]; Solve[Sys_Mag]; SaveSolution[Sys_Mag];
    }
  }
}



PostProcessing{  
  { Name MagSta; NameOfFormulation AMagneto;
    Quantity {
      { Name a;
        Value {
          Term { [ {a} ]; In Dom_VecPotA; Jacobian Vol; }
        }
      }
  /*    { Name az;
        Value {
          Term { [ CompZ[{a}] ]; In Dom_VecPotA; Jacobian Vol; }
        } 
      } */
      { Name b;
        Value {
          Term { [ {d a} ]; In Dom_VecPotA; Jacobian Vol; }
        }
      }
      { Name h;
        Value {
          Term { [ nu[] * {d a} ]; In Dom_VecPotA; Jacobian Vol; }
        }
      }
      { Name js;
        Value {
          Term { [ {js} ]; In Dom_VecPotA; Jacobian Vol; }
        }
      }
    }
  }
}



PostOperation{ 
  { Name Map_a; NameOfPostProcessing MagSta;
    Operation {
      Echo[ Str["l=PostProcessing.NbViews-1;", "View[l].IntervalsType = 1;",
			"View[l].NbIso = 40;"], File "tmp.geo", LastTimeStepOnly] ;
      Print[ a, OnElementsOf Dom_VecPotA, File "a.pos" ];
      Print[ js, OnElementsOf Dom_VecPotA, File "js.pos" ];
//      Print[ az, OnElementsOf Dom_VecPotA, File "az.pos" ];
      Print[ b, OnElementsOf Dom_VecPotA, File "b.pos" ];
    }
  }
}










