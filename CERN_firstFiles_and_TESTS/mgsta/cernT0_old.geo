/*********************************************************************
 *
 * CERN Test 0 Magnet
 *	Magnet design in Gmsh
 *
 * 	Author: Jorge Miaja Hernandez
 *
 *********************************************************************/

Include "cernT0_common.pro";
Include "m1_rectangle_macro.geo";

// Create boundary points 

Point(1) = {0, 0, 0, lc0};
Point(2) = {xDist_1st, 0, 0, lc_coils};
Point(3) = {xDist_1st+numberCoils*(thickness+gapBet), 0, 0, lc_coils};
Point(4) = {shieldIntR, 0, 0, lc_shield};
Point(5) = {shieldExtR, 0, 0, lc_shieldExt};
Point(6) = {0, shieldIntR, 0, lc_shield};
Point(7) = {0, shieldExtR, 0, lc_shieldExt};

If(Flag_infinityExpansion)
   Point(8) = {rInf, 0, 0, lcInf}; Point(9) = {rInfExt, 0, 0, lcInf};
   Point(10) = {0, rInf, 0, lcInf}; Point(11) = {0, rInfExt, 0, lcInf};   
EndIf

// Create boundary lines
Line(1) = {1,2}; Line(2) = {2,3}; Line(3) = {3,4}; 
Circle(4) = {4, 1, 6}; Line(5) = {6,1}; Line(6) = {4,5}; 
Circle(7) = {5, 1, 7}; Line(8) = {7,6};

Curve Loop(9) = {1,2,3,4,5}; Curve Loop(10) = {6,7,8,-4};

Compound Curve{1,2,3};

If(Flag_infinityExpansion)
   Line(11) = {5,8}; Circle(12) = {8,1,10}; Line(13) = {10,7};
   Line(14) = {8,9}; Circle(15) = {9,1,11}; Line(16) = {11,10};

   Curve Loop(17) = {11,12,13,-7}; Curve Loop(18) = {14,15,16,-12};
EndIf


// Call defined function to create rectangular coils
// assign nonvariable values
m1_dx = thickness;
m1_y = yDist;
m1_dy = height;
m1_z = 0;
m1_lc = lc_coils;
Holes[] = {};
coilSurf[] = {};

For nRectangle In {0:numberCoils-1} // begin for loop
	// assign variable value
	m1_x = xDist_1st + (nRectangle)*(m1_dx+gapBet);

	Call m1_rectangle_macro ;
	// Printf("iteration %g", nRectangle);
	Holes[] += m1_ll1;
	coilSurf[] += m1_surf;
EndFor // end for loop

S1 = news; Plane Surface(news) = {9, Holes[]};
S2 = news; Plane Surface(news) = {10};

If(Flag_infinityExpansion)
   S3 = news; Plane Surface(news) = {17};
   S4 = news; Plane Surface(news) = {18};
EndIf



// Physical regions
If(Flag_infinityExpansion)
   // Neumann boundary - simmetry along x axis
   Physical Curve("xaxis_ht0Neu", xaxis_ht0Neu) = {1,2,3,6,11,14}; 
   // Diritchlet boundary - simmetry along y axis
   Physical Curve("yaxis_bn0Dir", yaxis_bn0Dir) = {5,8,13,16}; 
   // Infinity region + air
   Physical Curve("surfInf", surfInf) = {15};
   Physical Surface("Air", Air) = {S1, S3}; // Air
   Physical Surface("AirInf", AirInf) = {S4};
Else
   Physical Surface("Air", Air) = {S1}; // Air
   // Neumann boundary - simmetry along x axis
   Physical Curve("xaxis_ht0Neu", xaxis_ht0Neu) = {1,2,3,6}; 
   // Diritchlet boundary - simmetry along y axis
   Physical Curve("yaxis_bn0Dir", yaxis_bn0Dir) = {5,8}; 
EndIf


// Treat all coils as one single phys surface
Physical Surface("coils_surface", coils_surface) = {coilSurf[]};
Physical Surface("ironYoke", ironYoke) = {S2}; // Iron yoke






