/*********************************************************************
 *
 * CERN Test 0 Magnet
 *
 *	structure, dimensions and global variables
 *
 * 	Author: Jorge Miaja Hernandez
 *
 *********************************************************************/

// Variables
mm = 1e-03;
cm = 1e-02;
kAmp = 1e3;


// MAGNET DIMENSIONS - in meters

// SHIELD
shieldIntR = 0.06;
shieldExtR = 0.1;

// COILS
numberCoils = 16; // number of coils == number of coils
height = 15e-3; // coil height
thickness = 15e-04; // coil thickness
xDist_1st = 15e-03; // distance along x-axis to origin 
yDist = 1e-03; // distance to x-axis
gapBet = 5e-4; // gap in between neighbouring coils

// INFINITY RING
rInf = 0.11;
rInfExt = 0.115;
lcInf = (rInfExt - rInf) / 5;

// Used in .geo and .pro files
DefineConstant[
	Flag_infinityExpansion = {1, Choices{0, 1}, 
		Name "Flags/Infinity mapping region"}
]; 



// CHARACTERISTIC LENGTHS

s = DefineNumber[1, Name "Model parameters/Global mesh size", 
	Help "Reduce for finer mesh, increase for coarser one"];
	
lc0 = xDist_1st / 5 * s; // element length around the origin
lc_coils = thickness / 1 * s; // element length around the coils
lc_shield = (shieldIntR - xDist_1st - numberCoils*(thickness+gapBet) ) 
		/ 5 * s; // element length near shield
lc_shieldExt = (shieldExtR - shieldIntR) / 5 * s;


// PHYSICAL REGIONS

//curves
xaxis_ht0Neu = 1000;
yaxis_bn0Dir = 1001;
surfInf = 1002;

//surfaces
Air = 1100;
AirInf = 1101;
coils_surface = 1102;
ironYoke = 1103;




