/*********************************************************************
 *
 * CERN Test 1 Magnet
 *
 *	structure, dimensions and global variables
 *
 *      Author: Jorge Miaja Hernandez
 *
 *	03.12.18 - Flag for infinity region not implemented in .geo
 * 	 
 *
 *********************************************************************/

// Variables
mm = 1e-03;
cm = 1e-02;


// MAGNET DIMENSIONS - in meters

// SHIELD
shieldIntR = 60*mm;
shieldExtR = 100*mm;

// COILS {down, top}
numberCoils = {14,18}; // number of coils
height = 15*mm; // coil height
thickness = 1.5*mm; // coil thickness
xDist_1st = {15*mm,7*mm}; // distance along x-axis to origin 
yDist = {1*mm, height + 2*mm}; // distance to x-axis
gapBet = 0.5*mm; // gap in between neighbouring coils


// INFINITY RING - NOT IMPLETENTED IN .GEO GILE YET
rInf = 0.11;
rInfExt = 0.115;
lcInf = (rInfExt - rInf) / 5;



// CHARACTERISTIC LENGTHS

s = DefineNumber[1, Name "Model parameters/Global mesh size", 
	Help "Reduce for finer mesh, increadse for coarser one"];
// Not implemented yet in .geo file
Flag_infinityExpansion = 0; //Inactive: value = 0; active: value = 1

lc0 = xDist_1st[0] / 5 * s; // element length around the origin
lc_coils = thickness / 1 * s; // element length around the coils
lc_shield = (shieldIntR - xDist_1st[0] - numberCoils[0]*(thickness+gapBet) ) 
		/ 5 * s; // element length near shield
lc_shieldExt = (shieldExtR - shieldIntR) / 5 * s;


// PHYSICAL REGIONS

//curves
xaxis_ht0Neu = 1000;
yaxis_bn0Dir = 1001;
surfInf = 1002;

//surfaces
Air = 1100;
AirInf = 1101;
coils_surface = 1102;
ironYoke = 1103;





