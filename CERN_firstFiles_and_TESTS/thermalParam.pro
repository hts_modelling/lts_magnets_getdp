/* ********************************************************************
 *
 * CERN FEM thermal parameters calculation
 *	Specification of thermal parameters 
 * 
 *  Material constants
 *
 * 	Author: Jorge Miaja Hernandez
 *
 ******************************************************************** */
 
 // Thermal parameters definition: (at 1.9K)
 // Copper
 rho_copper = 1.5392e-8; // Copper at 20C = 293K - 1.68e-8 Ohm m
 thermalCond_copper = 300;
 Cp_copper = 500;
 
 // Niobium Tin - Nb3Sn
 rho_NiobTin = 0;
 thermalCond_NiobTin = 300;
 Cp_NiobTin = 250;
 
 // Glass fiber (G10)
 rho_fiber = 0;
 thermalCond_fiber = 0.01;
 Cp_fiber = 750;
 
 // Strand NiobiumTin ratio
 nonCu_fraction = 0.4;
 
 
 // Determine areas for calculating the average
 A_coil = height * thickness;
 
 strand_diam = 0.75*mm; 
 nStrands = 40; 
 A_strands = nStrands * Pi * ( strand_diam / 2 ) ^ 2;
 A_total = (1e-4 + height)*(1e-4 + thickness);
 A_filling = A_coil - A_strands;
 A_insulation = A_total - A_coil;
 
 // Relative areas!!!
 AsR = A_strands / A_total;
 AfR = A_filling / A_total;
 AiR = A_insulation / A_total;
 
 
 // Setting insulation parameters depending on the selcted material
 If(Flag_Insulation==0)
	rho_ins = 0;
	thermalCond_ins = 0;
	Cp_ins = 0;
 EndIf
 
 If(Flag_Insulation==1)
	rho_ins = rho_fiber;
	thermalCond_ins = thermalCond_fiber;
	Cp_ins = Cp_fiber;
 EndIf
 
 
 
 // Setting void filling parameters depending on the selcted material
 If(Flag_filling==0)
	rho_filling = 0;
	thermalCond_filling = 0;
	Cp_filling = 0;
 EndIf
 
 If(Flag_filling==1)
	rho_filling = rho_fiber;
	thermalCond_filling = thermalCond_fiber;
	Cp_filling = Cp_fiber;
 EndIf
 
 
 
 // Set average/effective parameter values
 rhoEff = AsR*rho_NiobTin*nonCu_fraction + AsR*rho_copper*(1-nonCu_fraction)
		+ AfR*rho_filling + AiR*rho_ins;
// rhoEff = 0.0000000067547;
 thCondEff = AsR*( thermalCond_NiobTin*nonCu_fraction + thermalCond_copper*(1-nonCu_fraction))
		+ AfR*thermalCond_filling + AiR*thermalCond_ins;
 CpEff = AsR*( Cp_NiobTin*nonCu_fraction + Cp_copper*(1-nonCu_fraction) )
		+ AfR*Cp_filling + AiR*Cp_ins;
 
	
 
 
 
 
 
 
 
 