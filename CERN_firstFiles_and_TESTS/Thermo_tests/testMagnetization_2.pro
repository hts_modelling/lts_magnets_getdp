/* ********************************************************************
 *
 * CERN Testing eddy current calculation through magnetization process
 * 		of the superconductors
 *
 * The model tests if the eddy current formulation for superconducting 
 * materials has been properly modelled. As superconductor materials try
 * to expel all magnetic fields from within, even if there is no magnetic 
 * field variation. Our eddy current formulation is tested by applying a 
 * constant magnetic field in all regions of space and checking if the 
 * magnetic field lines avoid entering the block in the middle of the 
 * our domain. 
 * 	
 * 	Author: Jorge Miaja Hernandez
 *
 ******************************************************************** */

DefineConstant[ Flag_eddyForm = {0, Choices{0,1}, 
	Name "Flags/Eddy current formulation"},
	numIter = 2, timeO = 0, timeMax = 1, 
	deltaTime = timeMax / numIter ];

	
Group{ // regions used in the formulation of the problem
  // Physical regions to be referenced in functions 
  Air = Region[ 110 ];
  block = Region[ 111 ];
  contour = Region[ 100 ];


  // Abstract regions to be referenced in objects
  Vol_tot = Region[ {block, Air} ];
  Vol_block = Region [ block ];
  Sur_contour = Region[ contour ];
}


Function{ // Material laws
  mu0 = 4.e-7 * Pi;
  
  nu [ Region[{Air, block}] ]  = 1. / mu0;
  
  // Physical (and not abstract) regions as argument
  If(Flag_eddyForm)
//	boundaryA[] = X[] * $Time / timeMax;
	boundaryA[] = X[];
  Else
	boundaryA[] = X[];
  EndIf
  
  tau [ block ] = 1e+10;
}



Constraint{ // Boundary conditions
  { Name vectorPotA_contour; 
     Case { 
		{ Region Sur_contour; Type InitFromResolution;
			NameOfResolution init_a; }
    } 
    Case { // Create a constant magnetic field 
		If(Flag_eddyForm)
			{ Region Sur_contour; Type Assign; Value 1;
				TimeFunction boundaryA[]; }
		Else
			{ Region Sur_contour; Type Assign; Value boundaryA[]; }
		EndIf 
//		{ Region Vol_tot; Type InitFromResolution; NameOfResolution init_a; }
    }
/*	Case{ { Region Vol_tot; Type AssignFromResolution;
			NameOfResolution init_a; } } */
		
  }
}



Group{ // Function space domain
  Dom_VecPotA = Region[ {Vol_tot, Sur_contour} ];
}



FunctionSpace{ // where the solution is defined - FE expansion!!
   { Name VecPotA; Type Form1P;
       BasisFunction{ { // a(x,y) = sum_k aCoef_k sBF_k(x,y)
            Name sBF; NameOfCoef aCoef; Function BF_PerpendicularEdge;
            Support Dom_VecPotA; Entity NodesOf[All];
       } }
       Constraint{ { NameOfCoef aCoef; EntityType NodesOf; 
			NameOfConstraint vectorPotA_contour; } }
   }
}



Jacobian{ // mapping between mesh and reference elements
   { Name Vol;
	Case{ 
		{ Region All; Jacobian Vol; }
	} 
   }

   { Name Sur; // if non-homogeneous Neumann boundary conditions
	Case{ {
	   Region All; Jacobian Sur;
	} }
   }
}


Integration{ // Tell GetDP how to compute integrations
  { Name Int ;
    Case { { Type Gauss ;
             Case { { GeoElement Point       ; NumberOfPoints  1 ; }
					{ GeoElement Line        ; NumberOfPoints  4 ; }
					{ GeoElement Triangle    ; NumberOfPoints  16 ; }
                    { GeoElement Quadrangle  ; NumberOfPoints  4 ; }
			      }
           }
         }
  }
}



Formulation{ // FEM formulation of the problem

   { Name initA; Type FemEquation;
	Quantity { { Name a; Type Local; NameOfSpace VecPotA; } }
	Equation {
		Integral { [ nu[] * Dof{d a} , {d a} ]; 
		 In Vol_tot; Jacobian Vol; Integration Int; }
	}
   }


// curl nu curl A = J_source + J_eddy
// J_eddy = curl Magnetization
// Magnetization = -nu tau d/dt B
   { Name AMagneto; Type FemEquation; 
	Quantity {
	   { Name a; Type Local; NameOfSpace VecPotA; }
	}
	Equation {
		// Magnetostatics - no sources
		Integral { [ nu[] * Dof{d a} , {d a} ]; 
		 In Vol_tot; Jacobian Vol; Integration Int; }
//		Integral { DtDof[ nu[] * tau[] * Dof{d a}, {d a}]; 
//		 In Vol_block; Jacobian Vol; Integration Int;}
	}
   }
}



Resolution{ // what to do with the weak formulation
  { Name MagSta_a;
    System {
		{ Name Sys_Mag; NameOfFormulation AMagneto; }
    }
    Operation {
		If(Flag_eddyForm)
//			InitSolution[Sys_Mag];
			Generate[Sys_Mag]; Solve[Sys_Mag]; 
			PostOperation[Map_a];
			TimeLoopTheta[timeO, timeMax, deltaTime, 1]{
				Generate[Sys_Mag]; Solve[Sys_Mag]; 
				SaveSolution[Sys_Mag];
				Test[ $Time < timeMax ]{
					PostOperation[Map_a]; 
				}
			}
		Else
//			InitSolution[Sys_Mag];
			Generate[Sys_Mag]; Solve[Sys_Mag]; 
			SaveSolution[Sys_Mag];

		EndIf
    }
  }
  
    { Name init_a;
    System {
		{ Name Sys_initA; NameOfFormulation initA; DestinationSystem Sys_Mag;}
    }
    Operation {
		Generate Sys_initA; Solve Sys_initA; TransferSolution Sys_initA;
    }
  }
  
}



PostProcessing{  // Obtain physical quantities
  { Name MagSta; NameOfFormulation AMagneto;
    Quantity {
      { Name a;
        Value {
          Term { [ {a} ]; In Dom_VecPotA; Jacobian Vol; }
        }
      }
      { Name az;
        Value {
          Term { [ CompZ[{a}] ]; In Dom_VecPotA; Jacobian Vol; }
        } 
      } 
      { Name b;
        Value {
          Term { [ {d a} ]; In Dom_VecPotA; Jacobian Vol; }
        }
      }
      { Name h;
        Value {
          Term { [ nu[] * {d a} ]; In Dom_VecPotA; Jacobian Vol; }
        }
      }
//	  { Name xCoord;
//		Value{ Term{ [ X[] ]; In Sur_contour; Jacobian Vol; } } }  
		
    }
  }
}


PostOperation{ // What to do with these quantities
  { Name Map_a; NameOfPostProcessing MagSta;
    Operation {
      Print[ a, OnElementsOf Dom_VecPotA, File "a.pos" ];
      Print[ az, OnElementsOf Dom_VecPotA, File "az.pos" ];
      Print[ b, OnElementsOf Dom_VecPotA, File "b.pos" ];
//	  Print[ xCoord, OnElementsOf Sur_contour, Format Table,
//		File "xCoord.txt" ]; 
    }
  }
}











