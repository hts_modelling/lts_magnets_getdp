 Include "RRR.pro";

Function{ // rho(T, B);
	
// Possible fit models: NIST or CUDI
// Here we have used the NIST fit

// Some fit constants
P1 = 1.171e-17; P2 = 4.49; P3 = 3.841e10;	
P4 = 1.14; P5 = 50;	P6 = 6.428;	P7 = 0.4531;	  

a0 = -2.662; a1 = 0.3168; a2 = 0.6229;
a3 = -0.1839; a4 = 0.01827;


// rho with respect to temperature
c0_scale[] = c0 * RRR_value[$1, 4] / RRR_value[default_ref_Temp_RRR, 4];
rho_0[] = c0_scale[$1] / RRR;
rho_i[] = P1*($1^P2) / ( 1+P1*P3*( $1^(P2-P4) )*Exp[-((P5/$1)^P6)] );
rho_i0[] = P7*rho_i[$1]*rho_0[Tup_RRR] / (rho_i[$1]+rho_0[Tup_RRR]);
rho_N[] = rho_0[Tup_RRR] + rho_i[$1] + rho_i0[$1];

// include magnetic field dependance
logX[] = Log10[ c0 * $2 / rho_N[$1] ];
a_X[] = a0 + a1*logX[$1, $2] + a2*(logX[$1, $2]^2) + 
		a3*(logX[$1, $2]^3) + a4*(logX[$1, $2]^4);
corr[] = $2 > 0.01 ? 10^a_X[$1, $2] : 0 ;


// final rho
rho_NL[] = rho_N[ $1 ] * ( 1 + corr[ $1, Norm[$2] ] );

}