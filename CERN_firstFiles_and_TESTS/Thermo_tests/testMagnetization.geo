/*********************************************************************
 *
 * CERN Testing eddy current calculation through magnetization process
 * 		of the superconductors
 * The model tests if the eddy current formulation for superconducting 
 * materials has been properly modelled. As superconductor materials try
 * to expel all magnetic fields from within, even if there is no magnetic 
 * field variation. Our eddy current formulation is tested by applying a 
 * constant magnetic field in all regions of space and checking if the 
 * magnetic field lines avoid entering the block in the middle of the 
 * our domain. 
 *
 * 
 *
 *	Block design in Gmsh
 *
 * 	Author: Jorge Miaja Hernandez
 *
 *********************************************************************/

// Variables 
cm = 1e-2;
radius = 1;
blockHeight = 15*cm;
blockLength = 45*cm;
lc0 = blockLength / 5;
lcB = blockHeight / 2;

// Outside region
Point(1) = {0, 0, 0, lc0};
Point(2) = {radius, 0, 0, lc0};
Point(3) = {-radius, 0, 0, lc0};
Circle(1) =  {2, 1, 3}; Circle(2) =  {3, 1, 2};
Curve Loop(3) = {1,2}; // boundary

// Inside block
Point(4) = {-blockLength/2, -blockHeight/2, 0, lcB};
Point(5) = {blockLength/2, -blockHeight/2, 0, lcB};
Point(6) = {blockLength/2, blockHeight/2, 0, lcB};
Point(7) = {-blockLength/2, blockHeight/2, 0, lcB};
Line(4)={4,5}; Line(5)={5,6}; Line(6)={6,7}; Line(7)={4,7};
Curve Loop(8) = {4,5,6,-7};

Plane Surface(9) = {3, 8}; // air
Plane Surface(10) = {8}; // block

// Physical regions
Physical Curve ("contour", 100) = {1,2};
Physical Surface("Air", 110) = {9};
Physical Surface("block", 111) = {10}; 



