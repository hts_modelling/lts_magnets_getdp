/* ********************************************************************
 *
 * CERN Testing eddy current calculation through magnetization process
 * 		of the superconductors
 *
 * The model tests if the eddy current formulation for superconducting 
 * materials has been properly modelled. As superconductor materials try
 * to expel all magnetic fields from within, even if there is no magnetic 
 * field variation. Our eddy current formulation is tested by applying a 
 * constant magnetic field in all regions of space and checking if the 
 * magnetic field lines avoid entering the block in the middle of the 
 * our domain. 
 * 	
 * 	Author: Jorge Miaja Hernandez
 *
 ******************************************************************** */
DefineConstant[ 
	numIter = 500, timeO = 0, timeMax = 1, 
	initDeltaTime = 1e-10, minDeltaTime = 1e-10, maxDeltaTime = 0.005,
	relTol = 1e-4, absTol = 1e-8,
	stopWhen = 1e-5,
	relaxFactor = 1,
	NmaxIterations = 20,
	RRR = DefineNumber[100, 
			Name "Flags/99Thermal/4RRR"],
    default_ref_Temp_RRR = 273,
    Tup_RRR = DefineNumber[273, 
			Name "Flags/99Thermal/5RRR T high reference"],
    c0_scale = Tup_RRR / default_ref_Temp_RRR,
	c0 = DefineNumber[1.553e-8, Name "Flags/99Thermal/7c0"],
	bgB = DefineNumber[0, 
			Name "Flags/99Thermal/6Background field"],
	Flag_NL_coilProp  = 1,
	Flag_filling = 1, 
	Flag_Insulation = 0,
	deltaTime = timeMax / numIter ];
	
	
	
Include "testNL.pro";
/*
Include "rhoCu_NIST.pro";
Include "kCu_NIST.pro";
Include "CvCu_CUDI.pro";
Include "CvG10_NIST.pro";
Include "CvNb3Sn_NIST.pro"; 
*/
Include "coilProperties_NL.pro";
	
	
Group{ // regions used in the formulation of the problem
  // Physical regions to be referenced in functions 
  Air = Region[ 110 ];
  block = Region[ 111 ];
  contour = Region[ 100 ];


  // Abstract regions to be referenced in objects
  Vol_tot = Region[ {block, Air} ];
  Vol_block = Region [ block ];
  Sur_contour = Region[ contour ];
}


Function{ // Material laws
  initTemp = 1.9;
  Q_source[Vol_block] = 69000 / SurfaceArea[]; // 711 OR 
  Cp[Vol_block] = CpEff[$1, $2]; // CpNL[$1, $2];
  sigmaTher[Vol_block] = thCondEff[ $1, $2 ];
}



Constraint{ // Boundary conditions
  { Name initTemperature; Type Init;
    Case {
      { Region Vol_block; Value initTemp; }
    }
  }
}



Group{ // Function space domain
  Dom_T = Region[ {Vol_block} ];
}



FunctionSpace{ // where the solution is defined - FE expansion!!
   { Name NodalT; Type Form0;
       BasisFunction{ { // T(x,y) = sum_k tCoef_k tBF_k(x,y)
            Name tBF; NameOfCoef tCoef; Function BF_Node;
            Support Dom_T; Entity NodesOf[All];
       } }
       Constraint{ { 
            NameOfCoef tCoef; EntityType NodesOf; 
			NameOfConstraint initTemperature;
       } }
   }
}



Jacobian{ // mapping between mesh and reference elements
   { Name Vol;
	Case{ 
		{ Region All; Jacobian Vol; }
	} 
   }

   { Name Sur; // if non-homogeneous Neumann boundary conditions
	Case{ {
	   Region All; Jacobian Sur;
	} }
   }
}



Integration{ // Tell GetDP how to compute integrations
  { Name Int ;
    Case { { Type Gauss ;
             Case { { GeoElement Point       ; NumberOfPoints  1 ; }
					{ GeoElement Line        ; NumberOfPoints  4 ; }
					{ GeoElement Triangle    ; NumberOfPoints  16 ; }
                    { GeoElement Quadrangle  ; NumberOfPoints  4 ; }
			      }
           }
         }
  }
}



Formulation{ // FEM formulation of the problem
   { Name Thermal3B; Type FemEquation;
    Quantity {
		{ Name T; Type Local; NameOfSpace NodalT; }
	}
	Equation {
		Integral { DtDof[ Cp[{T}, bgB] * Dof{T}, {T} ]; 
		  In Vol_block; Jacobian Vol; Integration Int; }
		Integral { [ sigmaTher[{T}, bgB] * Dof{d T}, {d T} ]; 
		  In Vol_block; Jacobian Vol; Integration Int; }
		Integral { [ -Q_source[], {T} ];
		  In Vol_block; Jacobian Vol; Integration Int; }
	}
   }
}



Resolution{ // what to do with the weak formulation
  { Name MagSta_a;
    System {
		{ Name Sys_Mag; NameOfFormulation Thermal3B; }
    }
    Operation {
		InitSolution[Sys_Mag];
		SaveSolution[Sys_Mag];
/*		TimeLoopAdaptive[timeO, timeMax, initDeltaTime, 
		                 minDeltaTime, maxDeltaTime, "Euler", {}, 
						 System{{Sys_Mag, relTol, absTol, LinfNorm}}]{
			IterativeLoop[NmaxIterations, stopWhen, relaxFactor]{
					GenerateJac[Sys_Mag]; SolveJac[Sys_Mag]; }
		}{ SaveSolution[Sys_Mag]; }
		Flag_NL_coilProp = 1;  */

		
		TimeLoopTheta[timeO, 1e-10, 1e-10, 1]{
			IterativeLoop[NmaxIterations, stopWhen, relaxFactor]{
				GenerateJac[Sys_Mag]; SolveJac[Sys_Mag]; }
			SaveSolution[Sys_Mag];
		}
		TimeLoopTheta[1e-10, 1e-5, 1e-5 - 1e-10, 1]{
			IterativeLoop[NmaxIterations, stopWhen, relaxFactor]{
				GenerateJac[Sys_Mag]; SolveJac[Sys_Mag]; }
			SaveSolution[Sys_Mag];
		}
		TimeLoopTheta[1e-5, 0.005, 1e-5, 1]{
			IterativeLoop[NmaxIterations, stopWhen, relaxFactor]{
				GenerateJac[Sys_Mag]; SolveJac[Sys_Mag]; }
			SaveSolution[Sys_Mag];
		}
		TimeLoopTheta[0.005, timeMax, 0.005, 1]{
			IterativeLoop[NmaxIterations, stopWhen, relaxFactor]{
				GenerateJac[Sys_Mag]; SolveJac[Sys_Mag]; }
			SaveSolution[Sys_Mag];
		} 
		
		
		
    }
  }
}



PostProcessing{  // Obtain physical quantities
  { Name Ther; NameOfFormulation Thermal3B;
    Quantity {
      { Name T; 
        Value {
          Term { [ {T} ]; In Dom_T; Jacobian Vol; }
        }
      }
	  { Name Tav; 
		Value {
			Integral{ Type Global; [ {T} / SurfaceArea[] ]; 
				In Vol_block; Jacobian Vol; Integration Int; }
		}
	  }
	  { Name rho0; 
		Value {
			Integral{ Type Global; [rho_NL[{T},0]/SurfaceArea[]]; 
				In Vol_block; Jacobian Vol; Integration Int; }
		}
	  }
	  { Name rho5; 
		Value {
			Integral{ Type Global; [rho_NL[{T},5]/SurfaceArea[]]; 
				In Vol_block; Jacobian Vol; Integration Int; }
		}
	  }
	  { Name lgtb; 
		Value {
			Integral{ Type Global; [lgT[{T}, 0]/SurfaceArea[]]; 
				In Vol_block; Jacobian Vol; Integration Int; }
		}
	  }
	  { Name px; 
		Value {
			Integral{ Type Global; [p_X[{T}, 0]/SurfaceArea[]]; 
				In Vol_block; Jacobian Vol; Integration Int; }
		}
	  }
	  { Name cvg10; 
		Value {
			Integral{ Type Global; [CvG10_NL[{T}]/SurfaceArea[]]; 
				In Vol_block; Jacobian Vol; Integration Int; }
		}
	  }
	  { Name cvNiobTin; 
		Value {
			Integral{ Type Global; [CvSC_NL[{T}, bgB]/SurfaceArea[]]; 
				In Vol_block; Jacobian Vol; Integration Int; }
		}
	  }
	  { Name cvCu; 
		Value {
			Integral{ Type Global; [CvCu_NL_cudi[{T}]/SurfaceArea[]]; 
				In Vol_block; Jacobian Vol; Integration Int; }
		}
	  }
	  { Name kCu; 
		Value {
			Integral{ Type Global; 
			 [kCu_NL[{T}, rho_NL[{T},0], rho_NL[{T},bgB]]/SurfaceArea[]]; 
				In Vol_block; Jacobian Vol; Integration Int; }
		}
	  }
	  { Name cvMix; 
		Value {
			Integral{ Type Global; [Cp[{T}, bgB]/SurfaceArea[]]; 
				In Vol_block; Jacobian Vol; Integration Int; }
		}
	  }
	  { Name kMix; 
		Value {
			Integral{ Type Global; [sigmaTher[{T}, bgB]/SurfaceArea[]]; 
				In Vol_block; Jacobian Vol; Integration Int; }
		}
	  }
	  
	  
	  
	  
	  
	  
	  { Name cvg10t; 
		Value {
			Integral{ Type Global; [CvG10_NL[1.9]/SurfaceArea[]]; 
				In Vol_block; Jacobian Vol; Integration Int; }
		}
	  }
	  { Name cvNiobTint; 
		Value {
			Integral{ Type Global; [CvSC_NL[1.9, bgB]/SurfaceArea[]]; 
				In Vol_block; Jacobian Vol; Integration Int; }
		}
	  }
	  { Name cvCut; 
		Value {
			Integral{ Type Global; [CvCu_NL_cudi[1.9]/SurfaceArea[]]; 
				In Vol_block; Jacobian Vol; Integration Int; }
		}
	  }
	  { Name kCut; 
		Value {
			Integral{ Type Global; 
			 [kCu_NL[{T}, rho_NL[{T},0], rho_NL[1.9,bgB]]/SurfaceArea[]]; 
				In Vol_block; Jacobian Vol; Integration Int; }
		}
	  }
	  { Name cvMixt; 
		Value {
			Integral{ Type Global; [Cp[1.9, bgB]/SurfaceArea[]]; 
				In Vol_block; Jacobian Vol; Integration Int; }
		}
	  }
	  { Name kMixt; 
		Value {
			Integral{ Type Global; [sigmaTher[1.9, bgB]/SurfaceArea[]]; 
				In Vol_block; Jacobian Vol; Integration Int; }
		}
	  }
	  
    }
  }
}

PostOperation{ // What to do with these quantities
  { Name Map_t; NameOfPostProcessing Ther;
    Operation {
      Print[ T, OnElementsOf Dom_T, File ".pos" ];
      Print[ Tav[Vol_block], OnGlobal, Format Table, File "Tav.txt" ];
//	  Print[ rho0[Vol_block], OnGlobal, Format Table, File "rho0.txt" ];
// 	  Print[ rho5[Vol_block], OnGlobal, Format Table, File "rho5.txt" ];
//	  Print[ lgtb[Vol_block], OnGlobal, Format Table, File "lgtb.txt" ];
//	  Print[ px[Vol_block], OnGlobal, Format Table, File "px.txt" ];
	  Print[ cvg10[Vol_block], OnGlobal, Format Table, File "cvg10.txt" ];
	  Print[ cvNiobTin[Vol_block], OnGlobal, Format Table, File "cvNiobTin.txt" ];
	  Print[ cvCu[Vol_block], OnGlobal, Format Table, File "cvCu.txt" ];
	  Print[ kCu[Vol_block], OnGlobal, Format Table, File "kCu.txt" ];
	  Print[ cvMix[Vol_block], OnGlobal, Format Table, File "cvMix.txt" ];
	  Print[ kMix[Vol_block], OnGlobal, Format Table, File "kMix.txt" ];
	  
	  
	  Print[ cvg10t[Vol_block], OnGlobal, Format Table, File "init/cvg10.txt" ];
	  Print[ cvNiobTint[Vol_block], OnGlobal, Format Table, File "init/cvNiobTin.txt" ];
	  Print[ cvCut[Vol_block], OnGlobal, Format Table, File "init/cvCu.txt" ];
	  Print[ kCut[Vol_block], OnGlobal, Format Table, File "init/kCu.txt" ];
	  Print[ cvMixt[Vol_block], OnGlobal, Format Table, File "init/cvMix.txt" ];
	  Print[ kMixt[Vol_block], OnGlobal, Format Table, File "init/kMix.txt" ];
    }
  }
}











