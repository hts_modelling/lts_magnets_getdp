/* ********************************************************************
 *
 * CERN FEM thermal parameters calculation
 *	Specification of thermal parameters 
 * 
 *  Material constants
 *
 * 	Author: Jorge Miaja Hernandez
 *
 ******************************************************************** */
 
//-------------------------------------------------------------------------------
//----------------------------   FIT FUNCTIONS   --------------------------------
//-------------------------------------------------------------------------------

 Include "rhoCu_NIST.pro";
 Include "kCu_NIST.pro";
 Include "CvCu_CUDI.pro";
 Include "CvG10_NIST.pro";
 Include "CvNb3Sn_NIST.pro";

mm = 1e-3; 
height = 15*mm; // coil height
thickness = 1.5*mm; // coil thickness

Function{ 
 
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
 // Other thermal constants
 
 // Thermal parameters definition: (at 1.9K)
 // Copper
 rho_copper[] = Flag_NL_coilProp ? rho_NL[$1, $2] : 1.4334e-8;
 thermalCond_copper[] = Flag_NL_coilProp ? 
			kCu_NL[ $1, rho_NL[$1, 0], rho_NL[$1, $2] ]  :  300;
 Cp_copper[] = Flag_NL_coilProp ? CvCu_NL_cudi[$1] : 500;
 
 // Niobium Tin - Nb3Sn
 rho_NiobTin = 0;
 thermalCond_NiobTin = 300;
 Cp_NiobTin[] = Flag_NL_coilProp ? CvSC_NL[$1, $2] : 250;
 
 // Glass fiber (G10)
 rho_fiber = 0;
 thermalCond_fiber = 0.01;
 Cp_fiber[] = Flag_NL_coilProp ? CvG10_NL[$1] : 750;
 
  
  
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
 
 
 // Determine areas for calculating the average
 A_coil = height * thickness;
 
 strand_diam = 0.75*mm; 
 nStrands = 40; 
 A_strands = nStrands * Pi * ( strand_diam / 2 ) ^ 2;
 A_total = (1e-4 + height)*(1e-4 + thickness);
 A_filling = A_coil - A_strands;
 A_insulation = A_total - A_coil;
 // Strand NiobiumTin ratio ------------------
 nonCu_fraction = 0.4;
 
 // Setting insulation parameters depending on the selcted material
 If(Flag_Insulation==0)
	rho_ins = 0;
	thermalCond_ins = 0;
	Cp_ins[] = 0;
	A_tot = A_coil;
	A_increase = 1;
 EndIf
 
 If(Flag_Insulation==1)
	rho_ins = rho_fiber;
	thermalCond_ins = thermalCond_fiber;
	Cp_ins[] = Cp_fiber[$1];
	A_tot = A_total;
	A_increase = A_total / A_coil;
 EndIf
 
  // Relative areas!!!
 AsR = A_strands / A_tot;
 AfR = A_filling / A_tot;
 AiR = A_insulation / A_tot;
 
 
 
 // Setting void filling parameters depending on the selcted material
 If(Flag_filling==0)
	rho_filling = 0;
	thermalCond_filling = 0;
	Cp_filling[] = 0;
 EndIf
 
 If(Flag_filling==1)
	rho_filling = rho_fiber;
	thermalCond_filling = thermalCond_fiber;
	Cp_filling[] = Cp_fiber[$1];
 EndIf
 
 
 
 // Set average/effective parameter values
 rhoEff[] = (AsR*rho_NiobTin*nonCu_fraction + AsR*rho_copper[$1, $2]*(1-nonCu_fraction)
		+ AfR*rho_filling + AiR*rho_ins) * A_increase;
 thCondEff[] = (AsR*( thermalCond_NiobTin*nonCu_fraction + thermalCond_copper[$1, $2]
		*(1-nonCu_fraction)) + AfR*thermalCond_filling + AiR*thermalCond_ins) * A_increase;
 CpEff[] = (AsR*( Cp_NiobTin[$1, $2]*nonCu_fraction + Cp_copper[$1]*(1-nonCu_fraction) )
		+ AfR*Cp_filling[$1] + AiR*Cp_ins[$1]) * A_increase;
 
}
 
 
 
 
 
 