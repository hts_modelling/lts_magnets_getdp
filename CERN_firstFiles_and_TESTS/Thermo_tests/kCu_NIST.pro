Function{ // k_Cu(T, rho_Cu(T, 0), rho_Cu(T, B) );
	
// fit constants as specified in Internal Note
	beta = 0.634 / RRR;
    beta_r = beta / 0.0003;
    P1 = 1.754e-8;
    P2 = 2.763;
    P3 = 1102;
    P4 = -0.165;
    P5 = 70;
    P6 = 1.756;
    P7 = 0.838 / (beta_r^0.1661);

	W0[] = beta / $1;
    Wi[] = P1*($1^P2) / ( 1+P1*P3*( $1^(P2+P4) )*Exp[(-(P5/$1)^P6)] );
    Wi0[] = P7*Wi[$1]*W0[$1] / (Wi[$1] + W0[$1]);
	
//    kCu_NL[] = ( 1 / (W0[$1] + Wi[$1] + Wi0[$1]) ) *
//				rho_NL[$1, 0] / rho_NL[$1, $2];
				
	kCu_NL[] = ( 1 / (W0[$1] + Wi[$1] + Wi0[$1]) ) * $2 / $3;

}