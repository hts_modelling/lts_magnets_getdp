/*********************************************************************
 *
 * CERN general properties 
 *
 *	structure, dimensions and global variables
 *  properties shared by both pro and geo files, used by GetDp and gmsh
 *
 * 	Author: Jorge Miaja Hernandez
 *
 * Geometry values and parameters that need to be known by gmsh and getdp
 *
 *********************************************************************/
 
 // Used in .geo and .pro files
DefineConstant[
	Flag_magnetName = {0, Choices{0="T0 Magnet", 1="T1 Magnet"}, 
		Name "Flags/2Choose test magnet"},	
	Flag_infinityExpansion = {1, Choices{0, 1}, 
		Name "Geometry parameters/1Infinity mapping region/1Infinity region"},
	Flag_reducedAirMesh = {1, Choices{0,1},
		Name "Geometry parameters/1Infinity mapping region/3Reduced mesh", 
		Visible (Flag_infinityExpansion==1)}
]; 

// Variables
mm = 1e-03;
cm = 1e-02;
kAmp = 1e3;


// SHIELD
shieldIntR = 60*mm;
shieldExtR = 100*mm;

// COILS
height = 15*mm; // coil height
thickness = 1.5*mm; // coil thickness

s = DefineNumber[2, Name "Geometry parameters/1Global mesh size", 
	Help "Reduce for finer mesh, increase for coarser one"];

// INFINITY RING 
dInf = DefineNumber[2, Visible (Flag_infinityExpansion==1),
	Name "Geometry parameters/1Infinity mapping region/2Infinity region distance multiplier", 
	Help "The bigger this number is, the farther the infinity region mapping is"];
rInf = 0.5 * dInf;
rInfExt = rInf * 1.05;
lcInf = (rInfExt - rInf) / 10 * s;


// Geometrical number of coils_surface
If(Flag_magnetName==0)
	numberCoils = 16;
EndIf

If(Flag_magnetName==1)
	numberCoils = 32;
EndIf


nPoints = DefineNumber[3, 
		Name "Geometry parameters/1Infinity mapping region/4Number of points", 
		Help "Number of points to be embedded in Air region to force reduced mesh",
		Visible (Flag_infinityExpansion==1 && Flag_reducedAirMesh==1) ];
If(Flag_reducedAirMesh)
	distP = dInf*0.3;
	lc_reduced = 0.02 * s * dInf;
	angleP = Pi/(2 * (nPoints+1));
EndIf




// PHYSICAL REGIONS

// Curves
xaxis_ht0Neu = 1000;
yaxis_bn0Dir = 1001;
surfInf = 1002;
thermal_Neu = 1003;
// Although define here, Neumann boundary conditions are homogeneus, 
// so both terms are zero, and we could have not defined them


// Surfaces
Air = 1100;
AirInf = 1101;
coils_surface = 1102;
firstCoil = 1111;
ironYoke = 1103;





