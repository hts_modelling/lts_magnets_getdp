/* ********************************************************************
 *
 * CERN magnet FEM discretization
 *	Creates flags and determines values for each specific case
 * 
 *  Constants and file saving variable definitions
 *
 * 	Author: Jorge Miaja Hernandez
 *
 * All the different flags of the computational model are defined and 
 * initialized here, along with some model parameters and constants. 
 *
 * These constants could have been defined in the cernMagnets_common.pro 
 * file, they were, however, defined in this file if they will only be
 * used by getdp, gmsh has nothing to do with them.
 *
 ******************************************************************** */
 
 
 // FLAGS AND CONSTANTS
DefineConstant[
//----------------------------- FLAGS ----------------------------------
	// When adding a new case, both the section of code below as well as that in the 
	// resolution block have to be updated with the corresponding added new case
	// Also, do not forget to create the corresponding folder
	Flag_testCase = { 0, Choices{0="User defined test", 1="Test 0.B Diff Inductance",
		2="Test 1.A IFCC", 3="Test 0.A Diff Inductance. No iron yoke", 
		4="Test 5.A CERN T1. Diff Inductance.", 5="Test 2.A Ohmic Losses",
		6="Test 2.B Ohmic Losses with filling", 7="Test 2.C Ohm Losses. Ins. + filling",
		8="Test 3.A Resistivity and Tau IFCC", 9="Test 3.B Heat capacity", 
		10="Test 4.A LR Decay without losses"}, 
		Name "Flags/1Select test to run" 
	}, 
		
	Flag_singleIter = {1, Choices{1="Magnetostatic: Single iteration",
		0="Magnetodynamic: Ramp up excitation"}, Name "Flags/3Type of analysis",
		Visible (Flag_testCase==0) }, 
	Flag_ThermalForm = {0, Choices{0, 1}, Name "Flags/99Thermal/1Thermal analysis",
		Visible (Flag_testCase==0 && Flag_singleIter==0) }, 
	Flag_quench = {0, Choices{0="Always SC", 1="Quenched", 2="Quench function"}, 
		Name "Flags/99Thermal/2Thermal analysis",
		Visible (Flag_testCase==0 && Flag_ThermalForm==1) },
		
	// Circuit coupling 
	Flag_Cir = {0, Choices{0,1}, Name "Flags/3Circuit Coupling/1Active",
		Visible (Flag_testCase==0) },
	Flag_Cir_currentInput = {1, Choices{0="Voltage source", 1="Current current"}, 
		Name "Flags/3Circuit Coupling/2Input", Visible (Flag_testCase==0 && Flag_Cir) },
		
	// Determines if there are non linearities
	Flag_IronYokeNL	= {1, Choices{0,1}, Name "Flags/5Non linear iron yoke",
		Visible (Flag_testCase==0) },
	// Non linearity parameters
	Flag_NewtonMethod_NL = {1, Choices{0="Picard Banach method", 
		1="Newton Raphson method"}, Name "Flags/6Non linear method",
		Visible (Flag_IronYokeNL && Flag_testCase==0) },
	Flag_ifcc = {1, Choices{0,1}, Name "Flags/7IFCC", 
		Visible (Flag_singleIter==0 && Flag_testCase==0) },
		
	// Flags for thermal materials
	Flag_Insulation = {0, Choices{0="None", 1="Glass Fiber"}, 
		Name "Flags/99Thermal/3Insulation material", 
		Visible (Flag_testCase==0 && Flag_ThermalForm==1)},
	Flag_filling = {0, Choices{0="None", 1="Glass Fiber"}, 
		Name "Flags/99Thermal/4Void filling material", 
		Visible (Flag_testCase==0 && Flag_ThermalForm==1)},
	Flag_NL_coilProp = {0, Choices{0, 1}, 
		Name "Flags/8Non linear coil thermal properties",
		Visible (Flag_testCase==0) },
	
	// Other
	Flag_adaptiveTimeLoop = {0, Choices{0, 1}, 
		Name "Flags/9Time Loop/1Adaptive time loop resolution",
		Visible (Flag_testCase==0 && Flag_singleIter==0) },
		
//----------------------------------------------------------------------		
	// Default values 
	stopWhen = 1e-3,
	relaxFactor = 1,
	NmaxIterations = 20,
	
	// Time analysis - manetodynamic sparameters
	timeO = 0, // init time value
	timeMax = 1e-3, // ramp up function from 0 to timeMax second
	deltaTime = 20e-6, // with 20ms time step - 50 iterations (Tmax=1s)
	currentInit = 0, currentMax = 20*kAmp,
	
	// Time analysis - thermal adaptive loop
	initDeltaTime = 1e-10, minDeltaTime = 1e-10, 
/*	maxDeltaTime = DefineNumber[0.005, Name "Flags/9Time Loop/2Max dTime", 
       	Visible(Flag_adaptiveTimeLoop==1)],
	relTol = 1e-3, absTol = 1e-6,  */
	
	effCoilLength = 9.2, // only for circuit coupling
	effResCoilLength = 10.11, // only for circuit coupling
	twistPitch = 14*mm,
	SymmetryFactor = 4,
	
	// Thermal analysis variables
	initTemp = 1.9,
	
	// Non linear thermal material paraameter definition for NL files 
    RRR = DefineNumber[200, Name "Flags/99Thermal/5RRR", 
       	Visible(Flag_NL_coilProp==1  || Flag_testCase==8 || Flag_testCase==9) ],
    default_ref_Temp_RRR = 273,
    Tup_RRR = DefineNumber[295, Name "Flags/99Thermal/6RRR T high reference", 
       	Visible(Flag_NL_coilProp==1  || Flag_testCase==8 || Flag_testCase==9) ],
	// Verwij's excel 1.545 for RRR=R_273/R_4, =1.7 for RRR=R_290/R_4
/*	c0 = DefineNumber[1.553e-8, Name "Flags/99Thermal/7c0", 
       	Visible(Flag_NL_coilProp==1  || Flag_testCase==8 || Flag_testCase==9) ],*/
    //------------------------- 
	
	// Variables for tests 3A and 3B ONLY
	initTemp_1 = 1.9, initTemp_2 = 30, initTemp_3 = 100,
	initB_1 = 0, initB_2 = 5, initB_3 = 10,
	initB = {initB_1, Choices{initB_1="0", initB_2="5", initB_3="10"}, 
			Name "Flags/2Background field", Visible(Flag_testCase==9) },
	heatLoad = DefineNumber[1e6, Name "Flags/99Thermal/6T3B Heat Load", 
       	Visible(Flag_testCase==9) ],
	nTests = 3, // Cumulative (1:a, 2:ab, 3:abc)
	
	
	// Circuit coupling constants
	Rdump = 25e-3,
	timeExcitation = 5e-3,
	timeRampDown = 1e-4,
	tttt = timeExcitation + timeRampDown
	
];

//----------------------------------------------------------------------

filename = "";



//----------------------------------------------------------------------
//----------------------------- CASES ----------------------------------
//----------------------------------------------------------------------

If(Flag_testCase==0)  // User defined functions. Default values as predefined above
	folder2 = "";
ElseIf(Flag_testCase==1) // Test 0.B Differential Inductance (with iron yoke)
	// Set flags ----------------------
	Flag_magnetName = 0;
	Flag_IronYokeNL = 1;
	Flag_singleIter = 0;
	Flag_NewtonMethod_NL = 1;
	Flag_ifcc = 0;
	Flag_ThermalForm = 0;
	Flag_NL_coilProp = 0;
	Flag_Cir = 0;
	// Set constants ------------------
	timeMax = 1;
	deltaTime = 20e-3;
	timeVector={timeO, timeMax};   
// The 0 value is never used, but for some reason, a vector definition 
// 	in GetDP must have at least 2 elements
	deltaTimeVector={deltaTime, 0}; 
	nTimeWindows=1;
	currentMax = 20e3;
	folder2 = "T0B/";
ElseIf(Flag_testCase==2) // Tes 1.A Interfilament Coupling Currents 
	// Set flags ----------------------
	Flag_magnetName = 0;
	Flag_IronYokeNL = 1;
	Flag_singleIter = 0;
	Flag_NewtonMethod_NL = 1;
	Flag_ifcc = 1;
	Flag_ThermalForm = 0;
	Flag_NL_coilProp = 0;
	Flag_Cir = 0;
	// Set constants ------------------
	timeMax = 1e-3;
	deltaTime = 20e-6;
	timeVector={timeO, timeMax};   deltaTimeVector={deltaTime, 0};   nTimeWindows=1;
	currentMax = 1;
	folder2 = "T1A/";
ElseIf(Flag_testCase==3) // Test 0.A Differential Inductance (without iron yoke)
	// Set flags ----------------------
	Flag_magnetName = 0;
	Flag_IronYokeNL = 0;
	Flag_singleIter = 0;
	Flag_NewtonMethod_NL = 0;
	Flag_ifcc = 0;
	Flag_ThermalForm = 0;
	Flag_NL_coilProp = 0;
	Flag_Cir = 0;
	// Set constants ------------------
	timeMax = 1;
	deltaTime = 20e-3;
	timeVector={timeO, timeMax};   deltaTimeVector={deltaTime, 0};   nTimeWindows=1;
	currentMax = 20e3;
	folder2 = "T0A/";
ElseIf(Flag_testCase==4) // Test 5.A Differential Inductance (T1 with iron yoke)
	// Set flags ----------------------
	Flag_magnetName = 1;
	Flag_IronYokeNL = 1;
	Flag_singleIter = 0;
	Flag_NewtonMethod_NL = 1;
	Flag_ifcc = 0;
	Flag_ThermalForm = 0;
	Flag_NL_coilProp = 0;
	Flag_Cir = 0;
	// Set constants ------------------
	timeMax = 1;
	deltaTime = 20e-3;
	timeVector={timeO, timeMax};   deltaTimeVector={deltaTime, 0};   nTimeWindows=1;
	currentMax = 16e3;
	folder2 = "T5A/";
ElseIf(Flag_testCase==5) // Test 2.A Ohmic Losses - Bare Cable
	// Set flags ----------------------
	Flag_magnetName = 0;
	Flag_IronYokeNL = 1;
	Flag_singleIter = 0;
	Flag_NewtonMethod_NL = 1;
	Flag_ifcc = 0;
	Flag_ThermalForm = 1;
	Flag_NL_coilProp = 0;
	Flag_quench = 1; // forced quench
	Flag_Insulation = 0;
	Flag_filling = 0;
	Flag_Cir = 0;
	// Set constants ------------------
	timeMax = 1;
	deltaTime = 0.005;
	timeVector={timeO, timeMax};   deltaTimeVector={deltaTime, 0};   nTimeWindows=1;
	currentMax = 100;
	initTemp = 1.9;
	folder2 = "T2A/";
ElseIf(Flag_testCase==6) // Test 2.B - Bare Cable with Filler
	// Set flags ----------------------
	Flag_magnetName = 0;
	Flag_IronYokeNL = 1;
	Flag_singleIter = 0;
	Flag_NewtonMethod_NL = 1;
	Flag_ifcc = 0;
	Flag_ThermalForm = 1;
	Flag_NL_coilProp = 0;
	Flag_quench = 1; // forced quench
	Flag_Insulation = 0;
	Flag_filling = 1;
	Flag_Cir = 0;
	// Set constants ------------------
	timeMax = 1;
	deltaTime = 0.005;
	timeVector={timeO, timeMax};   deltaTimeVector={deltaTime, 0};   nTimeWindows=1;
	currentMax = 100;
	initTemp = 1.9;
	folder2 = "T2B/";
ElseIf(Flag_testCase==7) // Test 2.C - Insulated Cable with Filler
	// Set flags ----------------------
	Flag_magnetName = 0;
	Flag_IronYokeNL = 1;
	Flag_singleIter = 0;
	Flag_NewtonMethod_NL = 1;
	Flag_ifcc = 0;
	Flag_ThermalForm = 1;
	Flag_NL_coilProp = 0;
	Flag_quench = 1; // forced quench
	Flag_Insulation = 1;
	Flag_filling = 1;
	Flag_Cir = 0;
	// Set constants ------------------
	timeMax = 1;
	deltaTime = 0.005;
	timeVector={timeO, timeMax};   deltaTimeVector={deltaTime, 0};   nTimeWindows=1;
	currentMax = 100;
	initTemp = 1.9;
	folder2 = "T2C/";
ElseIf(Flag_testCase==8) // Test 3.A - Resistivity and Tau IFCC
	// Set flags ----------------------
	Flag_magnetName = 0;
	Flag_IronYokeNL = 1;
	Flag_singleIter = 0;
	Flag_NewtonMethod_NL = 1;
	Flag_ifcc = 0;
	Flag_ThermalForm = 0;
	Flag_NL_coilProp = 0;
	Flag_Insulation = 0;
	Flag_filling = 0;
	Flag_Cir = 0;
	// Set constants ------------------
	timeMax = 1;
	deltaTime = 0.02;
	timeVector={timeO, timeMax};   deltaTimeVector={deltaTime, 0};   nTimeWindows=1;
	currentMax = 20*kAmp;
	folder2 = "T3A/";
ElseIf(Flag_testCase==9) // Test 3.B - Heat Capacity 
	// Set flags ----------------------
	Flag_magnetName = 0;
	Flag_IronYokeNL = 1;
	Flag_singleIter = 0;
	Flag_NewtonMethod_NL = 1;
	Flag_ifcc = 0;
	Flag_ThermalForm = 0;
	Flag_NL_coilProp = 1;
	Flag_Insulation = 0;
	Flag_filling = 1;
	Flag_Cir = 0;
	// Set constants ------------------
	timeMax = 1;
	deltaTime = 0.005;
	timeVector = {timeO, 1e-10, 1e-5, deltaTime, timeMax};
	nTimeWindows = 4;
	deltaTimeVector = {1e-10, 1e-5 - 1e-10, 1e-5, deltaTime};
//	stopWhen = 1e-9;
//	maxDeltaTime = 0.005; // Adaptive loop
	initTemp = 1.9;
	folder2 = "T3B/";
ElseIf(Flag_testCase==10) // Test 4.A - LR Decay without losses
	// Set flags ----------------------
	Flag_magnetName = 0;
	Flag_IronYokeNL = 1;
	Flag_singleIter = 0;
	Flag_NewtonMethod_NL = 1;
	Flag_ifcc = 0;
	Flag_ThermalForm = 0;
	Flag_NL_coilProp = 0;
	Flag_Insulation = 0;
	Flag_filling = 0;
	Flag_Cir = 1;
	// Set constants ------------------
	timeMax = 0.498;
	timeVector = {timeO, 5e-3, 0.0055, 0.01, 0.0515, 0.149, 0.153, timeMax};
	deltaTimeVector = {5e-3/3, 1e-5, 1e-4, 0.0005, 0.0015, 0.004, 0.003};
	nTimeWindows = 7;
	currentMax = 18*kAmp;
	initTemp = 4.5;
	folder2 = "T4A/";
EndIf

//----------------------------------------------------------------------
//----------------------------------------------------------------------
//----------------------------------------------------------------------


Printf( "Flag_magnetName = %e", Flag_magnetName);
Printf( "Flag_IronYokeNL = %e", Flag_IronYokeNL);
Printf( "Flag_singleIter = %e", Flag_singleIter);
Printf( "Flag_NewtonMethod_NL = %e", Flag_NewtonMethod_NL);
Printf( "Flag_ifcc = %e", Flag_ifcc);
Printf( "Flag_ThermalForm = %e", Flag_ThermalForm);
Printf( "Flag_NL_coilProp = %e", Flag_NL_coilProp);
Printf( "Flag_Insulation = %e", Flag_Insulation);
Printf( "Flag_filling = %e", Flag_filling);
//Printf( "c0 = %e", c0);


If(Flag_singleIter)
	filename = "MagSta";
ElseIf(Flag_ifcc)
	filename = "ifcc";
Else	
	filename = "noIfcc";
EndIf



If(Flag_magnetName==0)
	folder = "T0_results/";
EndIf

If(Flag_magnetName==1)
	folder = "T1_results/";
EndIf


