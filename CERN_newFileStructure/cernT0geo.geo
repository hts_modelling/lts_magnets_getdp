/*********************************************************************
 *
 * CERN Test 0 Magnet
 *	Magnet design in Gmsh
 *
 * GEO files are not optimized and could be so by mixing T0 and T1 geo files 
 * in one single file with flags for the generation of the coils.
 * 
 * Update 13.02.19 - First coil is treated independently from the others
 *  
 * 	Author: Jorge Miaja Hernandez
 *
 *********************************************************************/

// Include "cernMagnets_common.pro";
Include "m1_rectangle_macro.geo";

// COILS
numberCoilsGeo = 16; // number of coils == number of coils ALSO IN COMMON.PRO
xDist_1st = 15e-03; // distance along x-axis to origin 
yDist = 1e-03; // distance to x-axis
gapBet = 5e-4; // gap in between neighbouring coils


// CHARACTERISTIC LENGTHS	
lc0 = xDist_1st / 10 * s; // element length around the origin
lc_coils = thickness / 10 * s; // element length around the coils
lc_shield = (shieldIntR - xDist_1st - numberCoilsGeo*(thickness+gapBet) ) 
		/ 10 * s; // element length near shield
lc_shieldExt = (shieldExtR - shieldIntR) / 10 * s;


// Create boundary points 

Point(1) = {0, 0, 0, lc0};
Point(2) = {xDist_1st, 0, 0, lc_coils};
Point(3) = {xDist_1st+numberCoilsGeo*(thickness+gapBet), 0, 0, lc_coils};
Point(4) = {shieldIntR, 0, 0, lc_shield};
Point(5) = {shieldExtR, 0, 0, lc_shieldExt};
Point(6) = {0, shieldIntR, 0, lc_shield};
Point(7) = {0, shieldExtR, 0, lc_shieldExt};

If(Flag_infinityExpansion)
   Point(8) = {rInf, 0, 0, lcInf}; Point(9) = {rInfExt, 0, 0, lcInf};
   Point(10) = {0, rInf, 0, lcInf}; Point(11) = {0, rInfExt, 0, lcInf};   
EndIf

// Create boundary lines
Line(1) = {1,2}; Line(2) = {2,3}; Line(3) = {3,4}; 
Circle(4) = {4, 1, 6}; Line(5) = {6,1}; Line(6) = {4,5}; 
Circle(7) = {5, 1, 7}; Line(8) = {7,6};

Curve Loop(9) = {1,2,3,4,5}; Curve Loop(10) = {6,7,8,-4};

Compound Curve{1,2,3};

If(Flag_infinityExpansion)
   Line(11) = {5,8}; Circle(12) = {8,1,10}; Line(13) = {10,7};
   Line(14) = {8,9}; Circle(15) = {9,1,11}; Line(16) = {11,10};

   Curve Loop(17) = {11,12,13,-7}; Curve Loop(18) = {14,15,16,-12};
EndIf


// Call defined function to create rectangular coils
// assign nonvariable values
m1_dx = thickness;
m1_y = yDist;
m1_dy = height;
m1_z = 0;
m1_lc = lc_coils;
Holes[] = {};
coilSurf[] = {};

For nRectangle In {0:numberCoilsGeo-1} // begin for loop
	// assign variable value
	m1_x = xDist_1st + (nRectangle)*(m1_dx+gapBet);

	Call m1_rectangle_macro ;
	// Printf("iteration %g", nRectangle);
	Holes[] += m1_ll1;
	coilSurf[] += m1_surf;
EndFor // end for loop

S1 = news; Plane Surface(news) = {9, Holes[]};
S2 = news; Plane Surface(news) = {10};

If(Flag_infinityExpansion)
   S3 = news; Plane Surface(news) = {17};
   S4 = news; Plane Surface(news) = {18};
   // Embedded point o force a coarser mesh in the external air region
   If(Flag_reducedAirMesh)
		For numbP In {1:nPoints}
			P1 = newp; 
			Point(P1) = {distP*Cos(angleP*numbP), 
						distP*Sin(angleP*numbP), 0, lc_reduced};
			Point{P1} In Surface{S3};
		EndFor
   EndIf
EndIf



// Physical regions
If(Flag_infinityExpansion)
   // Neumann boundary - simmetry along x axis
   Physical Curve("xaxis_ht0Neu", xaxis_ht0Neu) = {1,2,3,6,11,14}; 
   // Diritchlet boundary - simmetry along y axis
   Physical Curve("yaxis_bn0Dir", yaxis_bn0Dir) = {5,8,13,16}; 
   // Infinity region + air
   Physical Curve("surfInf", surfInf) = {15};
   Physical Surface("Air", Air) = {S1, S3}; // Air
   Physical Surface("AirInf", AirInf) = {S4};
Else
   Physical Surface("Air", Air) = {S1}; // Air
   // Neumann boundary - simmetry along x axis
   Physical Curve("xaxis_ht0Neu", xaxis_ht0Neu) = {1,2,3,6}; 
   // Diritchlet boundary - simmetry along y axis
   Physical Curve("yaxis_bn0Dir", yaxis_bn0Dir) = {5,8}; 
   // Outer boundary of iron yoke
   Physical Curve("surfInf", surfInf) = {7};
EndIf

// Physical surface of the first coil - interesting for testing
Physical Surface("firstCoil", firstCoil) = coilSurf[0]; 

// Treat all coils as one single phys surface
Physical Surface("coils_surface", coils_surface) = coilSurf[]; 
Physical Surface("ironYoke", ironYoke) = {S2}; // Iron yoke


