Function{ // CvG10(T);


  a0  = -2.4083;
  a1  = 7.6006;
  a2  = -8.2982;
  a3  = 7.3301;
  a4  = -4.2386;
  a5  = 1.4294;
  a6  = -0.24396;
  a7  = 0.015236;
  density = 1.9e3; // [kg/m^3] - mass density

  lgT[] = Log10[$1];
  p_X[] = a7*((lgT[$1])^7) + a6*((lgT[$1])^6) + a5*((lgT[$1])^5) +
		a4*((lgT[$1])^4) + a3*((lgT[$1])^3) + 
		a2*((lgT[$1])^2) + a1*((lgT[$1])) + a0; 
  CvG10_NL[] = density * 10^( p_X[ $1 ] );
  
}