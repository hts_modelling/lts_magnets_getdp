Function{ // RRR(Thigh, Tlow);

// Calculate RRR
T_ref_b0() = {0.0001,2,4,6,8,10,15,20,30,40,50,60,80,100,140,180,220,260,
	273,290,293,295,350,400,450,500,550,600,700,800,900,1000};
		
rho_ref_b0() = {5.71E-11,5.71E-11,5.71E-11,5.71E-11,5.72E-11,5.75E-11,
	6.00E-11,6.88E-11,1.30E-10,2.97E-10,6.02E-10,1.04E-09,2.22E-09,
	3.61E-09,6.49E-09,9.31E-09,1.20E-08,1.47E-08,1.56E-08,
	1.67E-08,1.69E-08,1.70E-08,2.06E-08,2.38E-08,2.69E-08,3.01E-08,
	3.32E-08,3.63E-08,4.26E-08,4.88E-08,5.50E-08,6.11E-08};
	
T_rho_Th() = ListAlt[ T_ref_b0(), rho_ref_b0() ];

RRR_value[] = InterpolationLinear[ $1 ]{ T_rho_Th() } /
		InterpolationLinear[ $2 ]{ T_rho_Th() };
}