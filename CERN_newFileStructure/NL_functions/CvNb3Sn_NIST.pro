Function{ // CvNb3Sn(T, B);
	
// Fit constants and parameters
  Tc0   = 17.8;   // [K]
  Bc20  = 27.012; // [T]
  T1 = 20; T2 = 400; 
  Tc[] = $1 < Bc20 ? Tc0*((1-$1/Bc20)^0.59) : 0;

// Fit coefficients
  a0    = 79.78547;     a1    = -247.44839;   a2    = 305.01434;
  a3    = -186.90995;   a4    = 57.48133;     a5    = -6.3977;
  a6    = -0.6827738;   a7    = 0.1662252;
  beta  = 1.241e-3;     density   = 8950; // [kg/m^3] 
  gamma = 0.138;        

  
  //  Polynomial approximation
  lgT2[] = Log10[$1];
  p_X2[] = 10^( a7*((lgT[$1]#2)^7) + a6*((#2)^6) + a5*((#2)^5) +
		a4*((#2)^4) + a3*((#2)^3) + a2*((#2)^2) + a1*((#2))+a0); 
  
// Most of the time the first conditional is true, since we are always below 
// 27 Tesla and this changes the value of Tc to something in between 10 and
// 17 Kelvin
  nb3snFactor[] = $1 < Tc[$2] ? (beta+3*gamma/(Tc0^2))*$1^3 + gamma*$2/Bc20*$1 :
				  $1 < T1 ? beta*($1^3) + gamma*$1 :
				  $1 < T2 ? p_X2[$1] : (234.89 + 0.0425*$1);
  
  CvSC_NL[] = density * nb3snFactor[$1, Norm[$2] ];
}