// Quench function
// Function of temperature and magnetic field that introduces
// joule losses on the superconducting material when it quenches
// and becomes resistive.

Function { // quenchState[ Is, Icrit]

x[] = 1 - ($2 / $1);

sharedCurrentValue[] = ( x[$1, $2]^2 ) * ( -2*x[$1, $2] + 3 );

sharedCurrent[] = x[$1, $2] <= 0 ? 0 :
				  x[$1, $2] >= 1 ? 1 : sharedCurrentValue[$1, $2];

quenchFct[] = $1 > 0 ? sharedCurrent[ $1, $2 ] : 
			 ($1==0 && $2>0) ? 0 : 1;
			  

quenchState[] = Flag_quench == 0 ? 0 :
				Flag_quench == 1 ? 1 : quenchFct[ $1, $2 ];
}