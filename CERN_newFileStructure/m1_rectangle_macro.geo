/*********************************************************************
 *
 * Macro
 *
 *	Macro for creating rectangles
 * 
 * 	Since variables in Gmsh are always PUBLIC, variable names in 
 * 	Macros have to be created in a way such that no other variable 
 * 	in other files could ever have the same name. We will use the 
 * 	prefix "m1_" for all variables in this file
 * 
 * Creates a rectangle parallel to plane xy at height z and z+ oriented
 * 	
 *
 *	Author: Jorge Miaja Hernandez
 *
 *********************************************************************/


Macro m1_rectangle_macro 

m1_p1=newp; 
Point(newp)={m1_x, m1_y, m1_z, m1_lc};

m1_p2=newp; 
Point(newp)={m1_x+m1_dx, m1_y, m1_z, m1_lc};

m1_p3=newp; 
Point(newp)={m1_x+m1_dx, m1_y+m1_dy, m1_z, m1_lc};

m1_p4=newp; 
Point(newp)={m1_x, m1_y+m1_dy, m1_z, m1_lc};

m1_l1=newl; Line(newl)={m1_p1,m1_p2};
m1_l2=newl; Line(newl)={m1_p2,m1_p3};
m1_l3=newl; Line(newl)={m1_p3,m1_p4};
m1_l4=newl; Line(newl)={m1_p4,m1_p1};

// z+ oriented Loop
m1_lines[] = {m1_l1, m1_l2, m1_l3, m1_l4};

m1_ll1 = newll; Curve Loop(m1_ll1) = {m1_lines[]};
m1_surf = news; Plane Surface(news) = {m1_ll1};

Return  // end of Function Def_Rectangle
