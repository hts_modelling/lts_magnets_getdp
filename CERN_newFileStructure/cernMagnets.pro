/* ********************************************************************
 *
 * CERN Main file
 *	Determines the type of analysis and manages all file relations
 * 	
 * 	Author: Jorge Miaja Hernandez
 *
 * This file only sets all group and spae definitions that will be used 
 * by GetDP, the group section links somehow the geometry in the .geo files 
 * to how getdp will understand this geometry
 *
 * The function group sets the value of mathematical constants or variables
 * of the physical model, such as constitutive material properties and 
 * other physical parameters, thermal constants, EM constants, input source
 * function...
 *
 ******************************************************************** */

Include "cernMagnets_common.pro";
Include "testCases.pro";
Include "coilProperties_NL.pro";
Include "NL_functions/ironBHcurve.pro";
Include "NL_functions/quench_fct.pro";



Group{ // regions used in the formulation of the problem
  // Physical regions to be referenced in functions 
  Air = Region[ Air ];
  Coils = Region[ coils_surface ];
  FirstCoil = Region[ firstCoil ];
  Shield = Region[ ironYoke ];
  Surface_ht0 = Region[ xaxis_ht0Neu ];
  Surface_bn0 = Region[ yaxis_bn0Dir ];
  Surface_Inf = Region[ surfInf ];
  // Circuit coupling regions and dummy numbers
  CircuitRdump = Region[ 8881 ];
  InputCurrent = Region[ 9991 ]; 

  // Abstract regions to be referenced in objects
  Vol_source = Region[ Coils ];
  Vol_coil = Region [ FirstCoil ];
  Vol_yoke = Region [ Shield ];
  Sur_Neu = Region[ {} ];
  Sur_Neu_T = Region[ {} ];
  Sur_Dir = Region[ {Surface_Inf, Surface_bn0} ];
  // Circuital abstrat region definition
  Cir_Resistance = Region[ CircuitRdump ];
  Cir_Admitance = Region[ {} ];
  Cir_Inductance = Region[ {} ]; 
  Vol_CirImpedance = Region[ {Cir_Resistance, Cir_Admitance, Cir_Inductance} ];
  Vol_CirSource = Region[ InputCurrent ]; 

  // With infinity mapping region
  If(Flag_infinityExpansion)
	AirInf = Region [ AirInf ];
	Vol_tot = Region[ {Air, AirInf, Coils, Shield} ];
	Vol_inf = Region[ AirInf ];
  Else
	Vol_tot = Region[ {Air, Coils, Shield} ];
  EndIf
}



Function{ // Material laws. Physical (and not abstract) regions as argument
  mu0 = 4.e-7 * Pi;
  // Found in CERN's note crosscheck for single iteration
  Current = 20*kAmp; //19.72*kAmp; 
  Icrit = 25*kAmp;
  
  If(Flag_IronYokeNL)
	// Found in CERN's note crosscheck nu[] = Interp[$1]{List[x, f(x)]};
	nu [ Shield ]  = nuIronYoke[$1];
	newtonJacobi [ Shield ] = dhdbForNewtonJacobi[$1];
  Else
	nu [ Shield ] = 1 / (1 * mu0);
  EndIf
  
  nu [ Region[{Air, FirstCoil, Coils}] ]  = 1. / mu0;
 
  If(Flag_infinityExpansion)
	nu [ AirInf ]  = 1. / mu0;
  EndIf
  
  // Source time function
  If(!Flag_Cir)
	source_Tfct[] = $Time / timeMax;
  Else // Circuit coupling time function
	source_Tfct[] = $Time <= timeExcitation ? 1 :
					$Time <= tttt ? (-$Time + tttt) / timeRampDown : 0 ;
  EndIf
  
  // Thermal source for tests 3B - see note crosscheck for value
  Q_source[Vol_source] = heatLoad; //   / SurfaceArea[];
  
  
// The kappa constant determines the ratio between the total area of all strands
// within the coil to the area of the coil's cross section. See thermalParam.pro
	kappa = A_strands / A_coil;
	
	rho[ Region[{Coils, FirstCoil}] ] = rhoEff[$1, $2];
	Cp[ Region[{Coils, FirstCoil}] ] = CpEff[$1, $2];
	sigmaTher[ Region[{Coils, FirstCoil}] ] = thCondEff[$1, $2];
	

	
  tauCoeff = ( mu0 * ( twistPitch/(2*Pi) )^2 ) ; // ??? /2????
  If(Flag_ifcc && !Flag_NL_coilProp)
	tau [ Region[{Coils, FirstCoil}] ] = 1e-3;
  ElseIf(Flag_ifcc && Flag_NL_coilProp)
	tau [ Region[{Coils, FirstCoil}] ] = 
					tauCoeff / ( rho_NL[$1, $2] * SurfaceArea[]{Coils} );
  Else
	tau [ Region[{Coils, FirstCoil}] ] = 0;
  EndIf
  
  // Circuit coupling parameters
  Resistance[ CircuitRdump ] = Rdump;
  
}


// Jacobian, integral, function space, formulation, resolution
// post processing and post operation
Include "cernFEMmodel.pro";

