/* ********************************************************************
 *
 * CERN magnet FEM discretization
 *	Sets domain and matrices to be solved
 *  Generats systme and algorithm/way to solve the matrices assembled 
 *  in the first stage
 *  Handles post-processing and post-operation as well 
 * 	
 * 	Author: Jorge Miaja Hernandez
 *
 * When getdp is run, it runs the specified resolution and then, the specified 
 * post-operation, these can be selected on a terminal by running:
* getdp cernMagnets.pro -solve 'Name of resolution' -pos 'Name of post-proc'
 * or on the GUI provided by OneLab, in this file, if many different resolutions
 * or post operations are defined, the one that appears first is the one used by 
 * default.
 *
 * In this document, there is only one resolution defined, that changes with the 
 * Flag values in the testCases.pro file.
 * There are many different PostOperations, which run the post-processing 
 * expressions defined in the PostProcessing block 
 *
 * For each specific Test Case, which have the name numbering and letter as those
 * in the note crosscheck between COMSOL and ANSYS, the post operation needed is 
 * already called upon during the resolution block, so the user must not do 
 * anything other than select the specific test case. For these cases, only the 
 * necessary output files will be created.
 *
 * The default post operation is DoNoth, which does nothing. (Remember that for 
 * all test cases, the needed post processing is called upon during the resolution)
 * If the user wants to see magnetic (or thermal) field lines and data, he/she 
 * should the Map_a (or Map_T) post operation
 *
 * Group, constraint and function space blocks in this file are needed to create
 * the mathematical elements that take part in the FEM formulation. Such as definition
 * of out space domains, finite elements and approximation series, as well as boundary
 * and initial conditions.
 *
 *
 ******************************************************************** */
//-------------------------------------------------------------------------------
//---------------------   Group and function definition  ------------------------
//-------------------------------------------------------------------------------


Group{ // Function space domain
	Dom_VecPotA = Region[ {Vol_tot, Sur_Neu, Vol_coil} ];
	Dom_thermalT = Region[ {Vol_source, Sur_Neu_T, Vol_coil} ];
	Dom_circuit = Region[ {Vol_CirSource, Vol_CirImpedance} ];
}


Constraint{ // Boundary conditions
  { Name Dir_a;
    Case { // Dirichlet conditions - Essential
      { Region Sur_Dir; Value 0; }
    }
  }
  
  { Name initTemperature; Type Init;
    Case { // Initial temperature for thermal analysis
      { Region Vol_source; Value initTemp; }
    }
  }
  
  // Circuit coupling constraints   
  { Name ElectricalCircuit; Type Network; 
    Case Circuit1 { // Describes node connection of branches
       { Region InputCurrent; Branch {1,2} ; }
       { Region Coils       ; Branch {2,3} ; }
       { Region CircuitRdump; Branch {3,1} ; }
    }
  }
  
  // Current through the coils 
  { Name Current_Coils ;
    Case {
	  If(Flag_singleIter)
	   { Region Vol_source; Value -Current; }
	  Else
       { Region Vol_source; Value -currentMax; TimeFunction source_Tfct[];}
	  EndIf
    }
  }
  
  { Name Voltage_Coils ;
    Case {
    }
  }
  
  // Circuit coupling constraint
  { Name Current_Cir ;
    Case {
       { Region InputCurrent; Value -currentMax; TimeFunction source_Tfct[];}
    }
  }
  
  { Name Voltage_Cir ;
    Case {
    }
  }
  
}
 

//-------------------------------------------------------------------------------
//----------------------   Jacobian plus integration   --------------------------
//-------------------------------------------------------------------------------
 
Jacobian{ // mapping between mesh and reference elements
   { Name Vol;
	Case{ 
	    If(Flag_infinityExpansion)	    
			{ Region All; Jacobian Vol; }
			{ Region Vol_inf; Jacobian VolSphShell {rInf, rInfExt}; }
	    Else
			{ Region All; Jacobian Vol; }
	    EndIf
	} 
   }

   { Name Sur; // if non-homogeneous Neumann boundary conditions
	Case{ {
		Region All; Jacobian Sur;
	} }
   }
}



Integration{ // Tell GetDP how to compute integrations
  { Name Int ;
    Case { { Type Gauss ;
             Case { { GeoElement Point       ; NumberOfPoints  1  ; }
					{ GeoElement Line        ; NumberOfPoints  4  ; }
					{ GeoElement Triangle    ; NumberOfPoints  16 ; }
                    { GeoElement Quadrangle  ; NumberOfPoints  4  ; }
			      }
           }
         }
  }
}



//-------------------------------------------------------------------------------
//----------------------------   FUNTION SPACE   --------------------------------
//-------------------------------------------------------------------------------



FunctionSpace{ // where the solution is defined - FE expansion!!
   { Name VecPotA; Type Form1P;
       BasisFunction{ { // a(x,y) = sum_k aCoef_k sBF_k(x,y)
            Name sBF; NameOfCoef aCoef; Function BF_PerpendicularEdge;
            Support Dom_VecPotA; Entity NodesOf[All];
       } }
       Constraint{ { 
            NameOfCoef aCoef; EntityType NodesOf; NameOfConstraint Dir_a;
       } }
   }

   { Name Is_region; Type Vector; 
       BasisFunction{ { // Is(x,y) = sum_k IsCoef_k iBF_(x,y) 
            Name iBF; NameOfCoef IsCoef; Function BF_RegionZ; 
			  Support Vol_source; Entity Vol_source; 
       } } 
       GlobalQuantity {
			{ Name Is; Type AliasOf; NameOfCoef IsCoef; }
			{ Name Us; Type AssociatedWith; NameOfCoef IsCoef; }
	   }
	   Constraint {
			{ NameOfCoef Us; EntityType Region; NameOfConstraint Voltage_Coils; }
			{ NameOfCoef Is; EntityType Region; NameOfConstraint Current_Coils; }
	   }
   }
   
   { Name NodalT; Type Form0;
       BasisFunction{ { // T(x,y) = sum_k tCoef_k tBF_k(x,y)
            Name tBF; NameOfCoef tCoef; Function BF_Node;
            Support Dom_thermalT; Entity NodesOf[All];
       } }
       Constraint{ { 
            NameOfCoef tCoef; EntityType NodesOf; 
			NameOfConstraint initTemperature;
       } }
   }
   
   { Name CircuitCoupling; Type Scalar; // For circuit equations
		BasisFunction { // I(x,y) = sum_k ir_k iBF_k(x,y)
			{ Name iBF ; NameOfCoef ir ; Function BF_Region ;
			Support Dom_circuit ; Entity Dom_circuit ; }
		}
		GlobalQuantity {
			{ Name Iz; Type AliasOf       ; NameOfCoef ir; }
			{ Name Uz; Type AssociatedWith; NameOfCoef ir; }
		}
		Constraint {
			{ NameOfCoef Iz; EntityType Region; NameOfConstraint Current_Cir; }
			{ NameOfCoef Uz; EntityType Region; NameOfConstraint Voltage_Cir; }
		}
   }
   
} 



//-------------------------------------------------------------------------------
//-----------------------------   FORMULATION   ---------------------------------
//-------------------------------------------------------------------------------

Formulation{ // FEM formulation of the problem
   { Name AMagneto; Type FemEquation; 
	Quantity {
	   { Name a; Type Local; NameOfSpace VecPotA; }
	   { Name js; Type Local; NameOfSpace Is_region; }
	   { Name Us; Type Global; NameOfSpace Is_region [Us]; }
	   { Name Is; Type Global; NameOfSpace Is_region [Is]; }
	   { Name T; Type Local; NameOfSpace NodalT; }
	   
	   // Circuit coupling only
	   { Name Iz; Type Global; NameOfSpace CircuitCoupling[Iz]; }
	   { Name Uz; Type Global; NameOfSpace CircuitCoupling[Uz]; }
	}
	Equation {
		Integral { [ nu[{d a}] * Dof{d a} , {d a} ]; 
		  In Vol_tot; Jacobian Vol; Integration Int; }
		If(Flag_NewtonMethod_NL && Flag_IronYokeNL)
			Integral { JacNL[ newtonJacobi[{d a}] * Dof{d a} , {d a} ]; 
			  In Vol_yoke; Jacobian Vol; Integration Int; }
		EndIf
		Integral { DtDof[kappa*nu[{d a}]*tau[{T}, {d a}]*Dof{d a},{d a}];
		  In Vol_source; Jacobian Vol; Integration Int; }
	    Integral { [ -numberCoils/SurfaceArea[]*Dof{js} , {a} ];
		  In Vol_source; Jacobian Vol; Integration Int; }
		  
		GlobalTerm { [ Dof{Us}, {Is} ] ; In Vol_source ; } 
		
		Galerkin { [ -numberCoils/SurfaceArea[] * Dof{js}, {a} ];
          In Vol_source; Jacobian Vol; Integration Int; }
        Galerkin { DtDof [ effCoilLength * numberCoils / SurfaceArea[] * Dof{a}, {js} ];
          In Vol_source; Jacobian Vol; Integration Int; }
        Galerkin { [ quenchState[1,1] * rho[{T}, {d a}] / SurfaceArea[] * Dof{js}, {js} ];
          In Vol_source; Jacobian Vol; Integration Int; }
		GlobalTerm { [ Dof{Us} / SymmetryFactor, {Is} ]; In Vol_source; }
		  
		// Circuit coupling equations
		If(Flag_Cir)
			GlobalTerm { NeverDt[ Dof{Uz}, {Iz} ]; In Cir_Resistance; }
			GlobalTerm { NeverDt[ Resistance[]*Dof{Iz}, {Iz} ]; In Cir_Resistance; }
			GlobalEquation{
				Type Network; NameOfConstraint ElectricalCircuit;
				{ Node {Is}; Loop {Us}; Equation {Us}; In Vol_source; }
				{ Node {Iz}; Loop {Uz}; Equation {Uz}; In Dom_circuit; }
			}
		EndIf
	}
   }
   
   
   { Name Thermal; Type FemEquation;
    Quantity {
		{ Name T; Type Local; NameOfSpace NodalT; }
		{ Name a; Type Local; NameOfSpace VecPotA; }
	    { Name js; Type Local; NameOfSpace Is_region; }
		{ Name Is; Type Global; NameOfSpace Is_region [Is]; }
	}
	Equation {
		Integral { DtDof[ Cp[{T},{d a}] * Dof{T}, {T} ]; 
		  In Vol_source; Jacobian Vol; Integration Int; }
		Integral { [ sigmaTher[{T},{d a}] * Dof{d T}, {d T} ]; 
		  In Vol_source; Jacobian Vol; Integration Int; }
		Integral { [ -kappa*nu[{d a}]*tau[{T},{d a}]*SquNorm[Dt[{d a}]],{T}];
		  In Vol_source; Jacobian Vol; Integration Int; }
		Integral { [ -quenchState[1,1] * rho[{T}, {d a}] * 
				  SquNorm[ {js} * numberCoils / SurfaceArea[] ] ,{T}]; 
		  In Vol_source; Jacobian Vol; Integration Int; } // Abs[{Is}], Icrit
	}
   }
   
   
   // Formulation only useful for Test 3B
   { Name Thermal3B; Type FemEquation;
    Quantity {
		{ Name T; Type Local; NameOfSpace NodalT; }
	}
	Equation {
		Integral { DtDof[ Cp[{T}, initB] * Dof{T}, {T} ]; 
		  In Vol_source; Jacobian Vol; Integration Int; }
		Integral { [ sigmaTher[{T}, initB] * Dof{d T}, {d T} ]; 
		  In Vol_source; Jacobian Vol; Integration Int; }
		Integral { [ -Q_source[], {T} ];
		  In Vol_source; Jacobian Vol; Integration Int; }
	}
   }
   
   
}






//-------------------------------------------------------------------------------
//------------------------------   RESOLUTION   ---------------------------------
//-------------------------------------------------------------------------------


Resolution{ // what to do with the weak formulation
  { Name CERN_Magnets;
    System {
		If(Flag_testCase==9)
			{ Name Sys_T3b ; NameOfFormulation Thermal3B ; }
		Else
			{ Name Sys_Mag; NameOfFormulation AMagneto; }
			{ Name Sys_Ther; NameOfFormulation Thermal; }
		EndIf
    }
    Operation {
		If(Flag_testCase==9)
			InitSolution[Sys_T3b]; SaveSolution[Sys_T3b];
/*			TimeLoopAdaptive[timeO, timeMax, initDeltaTime, 
		                 minDeltaTime, maxDeltaTime, "Euler", {maxDeltaTime}, 
						 System{{Sys_T3b, relTol, absTol, LinfNorm}}]{
				IterativeLoop[NmaxIterations, stopWhen, relaxFactor]{
					GenerateJac[Sys_T3b]; SolveJac[Sys_T3b]; }
			}{ SaveSolution[Sys_T3b]; }  */
			
			// Piece-wise time step simulation
			For i In {0:nTimeWindows-1}
				TimeLoopTheta[timeVector(i), timeVector(i+1), deltaTimeVector(i), 1]{
					IterativeLoop[NmaxIterations, stopWhen, relaxFactor]{
						GenerateJac[Sys_T3b]; SolveJac[Sys_T3b]; }
					SaveSolution[Sys_T3b];
				}
			EndFor
			
//-------------------------------------------------------------------------------
			
		ElseIf(Flag_singleIter)
			If(Flag_IronYokeNL) 
				IterativeLoop[NmaxIterations, stopWhen, relaxFactor]{
					GenerateJac[Sys_Mag]; SolveJac[Sys_Mag]; }
			Else
				Generate[Sys_Mag]; Solve[Sys_Mag];
			EndIf
				SaveSolution[Sys_Mag];
				
		Else // Ramp up time analysis
			InitSolution[Sys_Mag]; InitSolution[Sys_Ther];
		  For i In {0:nTimeWindows-1}
			TimeLoopTheta[timeVector(i), timeVector(i+1), deltaTimeVector(i), 1]{
				If(Flag_IronYokeNL) 
					IterativeLoop[NmaxIterations, stopWhen, relaxFactor]{
						GenerateJac[Sys_Mag]; SolveJac[Sys_Mag]; }
				Else
					Generate[Sys_Mag]; Solve[Sys_Mag];
				EndIf
				SaveSolution[Sys_Mag]; 
				If(Flag_ThermalForm)
					Generate[Sys_Ther]; Solve[Sys_Ther]; SaveSolution[Sys_Ther];
				EndIf
			}
		  EndFor
		EndIf

//-------------------------------------------------------------------------------
		
		// CALL POST PROCESSING 
		If(Flag_testCase==0)  
			PostOperation[Flux]; PostOperation[B_HT0];
// Tests 0.A and 0.B Differential Inductance (with and without iron yoke)
		ElseIf(Flag_testCase==1 || Flag_testCase==3 || Flag_testCase==4 
								|| Flag_testCase==10) 
			PostOperation[Flux]; PostOperation[B_HT0];
		ElseIf(Flag_testCase==2) // Test 1.A Interfilament Coupling Currents 
			PostOperation[Mifcc];
// Tests 2.A 2.B and 2.C Ohmic losses with thermal simulation and different configs.
		ElseIf(Flag_testCase>4 && Flag_testCase<8) 
			PostOperation[OhmicLosses];
		ElseIf(Flag_testCase==8) // Test 3A
			PostOperation[ResistivityTauIfcc];  
		ElseIf(Flag_testCase==9) // test 3B
			PostOperation[Map_T]; PostOperation[HeatCapacity_T3B];
		EndIf
    }
  }

}




//-------------------------------------------------------------------------------
//---------------------------   Post processing   -------------------------------
//-------------------------------------------------------------------------------

PostProcessing{  // Obtain physical quantities
  { Name Mag; NameOfFormulation AMagneto;
    Quantity {
      { Name a;
        Value {
          Term { [ {a} ]; In Dom_VecPotA; Jacobian Vol; }
        }
      }
      { Name az;
        Value {
          Term { [ Abs[CompZ[{a}]] ]; In Dom_VecPotA; Jacobian Vol; }
        } 
      } 
      { Name b;
        Value {
          Term { [ {d a} ]; In Dom_VecPotA; Jacobian Vol; }
        }
      }
	  { Name b_norm; Value { Term{ [ Norm[{d a}] ]; 
			In Dom_VecPotA; Jacobian Vol; Integration Int; } } }
      { Name h;
        Value {
          Term { [ nu[{d a}] * {d a} ]; In Dom_VecPotA; Jacobian Vol; }
        }
      }
      { Name js;
        Value {
          Term { [ numberCoils/SurfaceArea[]*{js} ]; In Dom_VecPotA; Jacobian Vol; }
        }
      }
    }
  }
  
  { Name Thermal; NameOfFormulation Thermal;
    Quantity {
      { Name T; Value {
          Term { [ {T} ]; In Dom_thermalT; Jacobian Vol; }
        }
      }
	  { Name temp_HT0; 
		Value {
			Integral{ Type Global; [ {T} / SurfaceArea[] ]; 
				In Vol_coil; Jacobian Vol; Integration Int; }
		}
	  }
	}
  }
  
  { Name MagFlux; NameOfFormulation AMagneto;
    Quantity {
	  /* Get the flux linkage by integrating the vector potential A 
	  over the surface of the coils and dividing the result by its area.
	  This computes the average vector potential  */
	  { Name fluxLinkage; 
		Value { Integral{ Type Global; [ SymmetryFactor * numberCoils * 
											Norm[{a}] / SurfaceArea[] ]; 
			In Vol_source; Jacobian Vol; Integration Int; } 
		} 
	  }
	  { Name diffInd; 
		Value { Integral{ Type Global; [ (SymmetryFactor * numberCoils * 
						Dt[Norm[{a}]] / SurfaceArea[]) / Dt[Norm[{js}]] ];
			In Vol_source; Jacobian Vol; Integration Int; } 
		} 
	  }
	  { Name current; 
		Value { Integral{ Type Global; [ ( Norm[{js}]/SurfaceArea[] ) ]; 
			In Vol_source; Jacobian Vol; Integration Int; } 
		} 
	  }
	  { Name diffCurrent; 
		Value { Integral{ Type Global; [(Dt[Norm[{js}]]*SurfaceArea[])]; 
			In Vol_source; Jacobian Vol; Integration Int; } 
		} 
	  }  
	  
	  { Name IIs; 
		Value { Term { [ {Is} ]; In Vol_source; } } 
	  }
	  
	  { Name IIz; 
		Value { Term { [ {Iz} ]; In Dom_circuit; } } 
	  }
	  
    }
  }
  
  { Name Bfield; NameOfFormulation AMagneto;
    Quantity { 
		{ Name bField; Value { Integral{ Type Global; [ Norm[{d a}] / SurfaceArea[] ]; 
			In Vol_coil; Jacobian Vol; Integration Int; } } }
		{ Name b_ht0; Value { Local { [ {d a} ]; 
			In Vol_coil; Jacobian Vol; Integration Int;} } }
	}
  }
  
  { Name Mifcc; NameOfFormulation AMagneto;
    Quantity { 
	  { Name MifccXY; 
		Value { 
			Integral{ Type Global; [ kappa*nu[{d a}]*tau[{T},{d a}]*Dt[{d a}] 
															/ SurfaceArea[] ]; 
				In Vol_coil; Jacobian Vol; Integration Int; }
		} 
	  }
	  { Name Qifcc_HT0; 
		Value { 
			Integral{ Type Global; [ kappa*nu[{d a}]*tau[{T},{d a}]*SquNorm[Dt[{d a}]] ]; 
				In Vol_coil; Jacobian Vol; Integration Int; }
		} 
	  }
	  { Name Qifcc_tot; 
		Value { 
			Integral{ Type Global; [ SymmetryFactor * kappa * nu[{d a}] * 
										tau[{T},{d a}] * SquNorm[Dt[{d a}]] ]; 
				In Vol_source; Jacobian Vol; Integration Int; }
		} 
	  }
	}
  }
  
  { Name OhmicLosses; NameOfFormulation Thermal; 
    Quantity {
	  { Name rho_HT0; 
		Value {
			Integral{ Type Global; [ rho[{T},{d a}] / SurfaceArea[] ]; 
				In Vol_coil; Jacobian Vol; Integration Int; }
		}
	  }
	  { Name temp_HT0; 
		Value {
			Integral{ Type Global; [ {T} / SurfaceArea[] ]; 
				In Vol_coil; Jacobian Vol; Integration Int; }
		}
	  }
	  { Name Qjoule_HT0; 
		Value {
			Integral{ Type Global; [quenchState[1,1]*rho[{T},{d a}]*
								SquNorm[ {js}/SurfaceArea[] ]]; 
				In Vol_coil; Jacobian Vol; Integration Int; }
		}
	  }
	  { Name Qjoule_tot; 
		Value {
			Integral{ Type Global; [SymmetryFactor*quenchState[1,1]*rho[{T},{d a}]*
								SquNorm[numberCoils/SurfaceArea[]*{js}]]; 
				In Vol_source; Jacobian Vol; Integration Int; }
		}
	  }
    }
  }
  
  { Name ResistTau; NameOfFormulation AMagneto; 
    Quantity {
	  { Name bField; Value { Integral{ Type Global; [ Norm[{d a}] / SurfaceArea[] ]; 
			In Vol_coil; Jacobian Vol; Integration Int; } } }
	For i In {1:nTests}
	  { Name rhoCu_HT0~{i}; 
		Value {
			Integral{ Type Global; [rho_NL[initTemp~{i},{d a}]/SurfaceArea[]]; 
				In Vol_coil; Jacobian Vol; Integration Int; }
		}
	  }
	  { Name rho_HT0~{i}; 
		Value {
			Integral{ Type Global; [ AsR*rho_NL[initTemp~{i}, {d a}]*(1-nonCu_fraction)
									/SurfaceArea[] ]; 
				In Vol_coil; Jacobian Vol; Integration Int; }
		}
	  }
	  { Name tau_HT0~{i}; 
		Value {
			Integral{ Type Global; [ tauCoeff / ( rho_NL[initTemp~{i}, {d a}]
										* SurfaceArea[] ) ]; 
				In Vol_coil; Jacobian Vol; Integration Int; }
		}
	  }
    EndFor
	
	}
  }
  
  { Name T3B; NameOfFormulation Thermal3B; // {T}, initB
    Quantity { 
		{ Name Tht0; Value { Integral{ Type Global; [ Norm[{T}] / SurfaceArea[] ]; 
			In Vol_coil; Jacobian Vol; Integration Int; } } }
			
		{ Name CvCu_ht0; Value { Integral{ Type Global; [ CvCu_NL_cudi[ {T} ] / SurfaceArea[] ]; 
			In Vol_coil; Jacobian Vol; Integration Int; } } }
		{ Name CvNb3Sn_ht0; Value { Integral{ Type Global; [ CvSC_NL[ {T}, initB ] / SurfaceArea[] ]; 
			In Vol_coil; Jacobian Vol; Integration Int; } } }
		{ Name CvG10_ht0; Value { Integral{ Type Global; [ CvG10_NL[ {T} ] / SurfaceArea[] ]; 
			In Vol_coil; Jacobian Vol; Integration Int; } } }
		{ Name Cv_Mix_ht0; Value { Integral{ Type Global; [ CpEff[ {T}, initB ] / SurfaceArea[] ]; 
			In Vol_coil; Jacobian Vol; Integration Int; } } }
			
		{ Name kCu_ht0; 
		  Value { Integral{ 
		    Type Global; [ kCu_NL[ {T}, rho_NL[{T}, 0], rho_NL[{T}, initB] ] / SurfaceArea[] ]; 
		    	In Vol_coil; Jacobian Vol; Integration Int; } 
		  } 
		}
		{ Name kMix_ht0; Value { Integral{ Type Global; [ sigmaTher[ {T}, initB ] / SurfaceArea[] ]; 
			In Vol_coil; Jacobian Vol; Integration Int; } } }
	}
  }
  
}


//-------------------------------------------------------------------------------


PostOperation{ // What to do with these quantities
  If(Flag_testCase==9)
    { Name DoNoth; NameOfPostProcessing T3B; Operation {} }
  Else
	{ Name DoNoth; NameOfPostProcessing Mag; Operation {} }
  EndIf
  { Name Map_a; NameOfPostProcessing Mag;
    Operation {
      Print[ a, OnElementsOf Dom_VecPotA, File StrCat[folder, folder2, "a.pos"] ];
      Print[ js, OnElementsOf Dom_VecPotA, File StrCat[folder, folder2, "js.pos"] ];
      Print[ az, OnElementsOf Dom_VecPotA, File StrCat[folder, folder2, "az.pos"] ]; 
      Print[ h, OnElementsOf Dom_VecPotA, File StrCat[folder, folder2, "h.pos"] ]; 
      Print[ b, OnElementsOf Dom_VecPotA, File StrCat[folder, folder2, "b.pos"] ];    
      Print[ b_norm, OnElementsOf Dom_VecPotA, File StrCat[folder, folder2, "b_norm.pos"] ];
    }
  }
  { Name Map_T; NameOfPostProcessing Thermal;
    Operation {
      Print[ T, OnElementsOf Dom_VecPotA, File StrCat[folder, folder2, "T.pos"] ];
	  Print[ temp_HT0[Vol_coil], OnGlobal, Format Table,
			File StrCat[folder, folder2, "t_ht0.txt"] ];
    }
  }
  
}


PostOperation{ // Test results  
	{ Name Flux; NameOfPostProcessing MagFlux;
		Operation {
			Print[ fluxLinkage[Vol_source], OnGlobal, Format Table,
				File StrCat[folder, folder2, "fluxTable.txt"] ];
			Print[ diffInd[Vol_source], OnGlobal, Format Table,
				File StrCat[folder, folder2, "diffIndTable.txt"] ];
//			Print[ current[Vol_source], OnGlobal, Format Table,
//				File StrCat[folder, folder2, "current.txt"] ];
//			Print[ diffCurrent[Vol_source], OnGlobal, Format Table,
//				File StrCat[folder, folder2, "diffCurrent.txt"] ];    
			Print[ IIs, OnRegion Vol_source, Format Table, 
					File StrCat[folder, folder2, "IIs.txt"] ];
			If(Flag_Cir)
				Print[ IIz, OnRegion InputCurrent, Format Table, 
					File StrCat[folder, folder2, "IIz.txt"] ];
			EndIf
		}
	}
	
	{ Name B_HT0; NameOfPostProcessing Bfield;
		Operation {
			Print[ bField[Vol_coil], OnGlobal, Format Table,
				File StrCat[folder, folder2, "B_HT0.txt"] ];
//			Print[ b_ht0, OnElementsOf Vol_coil, 
//				File StrCat[folder, folder2, "b_ht0.pos"] ];   
		}
	}
	
	{ Name Mifcc; NameOfPostProcessing Mifcc;
		Operation {
			Print[ MifccXY[Vol_coil], OnGlobal, Format Table,
				File StrCat[folder, folder2, "M_ifccXY.txt"] ];	
			Print[ Qifcc_HT0[Vol_coil], OnGlobal, Format Table,
				File StrCat[folder, folder2, "Q_ifcc_HT0.txt"] ];	
			Print[ Qifcc_tot[Vol_source], OnGlobal, Format Table,
				File StrCat[folder, folder2, "Q_ifcc_tot.txt"] ];	
		}
	}
	
	{ Name OhmicLosses; NameOfPostProcessing OhmicLosses;
		Operation {
			Print[ rho_HT0[Vol_coil], OnGlobal, Format Table,
				File StrCat[folder, folder2, "rho_ht0.txt"] ];
			Print[ temp_HT0[Vol_coil], OnGlobal, Format Table,
				File StrCat[folder, folder2, "t_ht0.txt"] ];
			Print[ Qjoule_HT0[Vol_coil], OnGlobal, Format Table,
				File StrCat[folder, folder2, "Qjoule_HT0.txt"] ];
			Print[ Qjoule_tot[Vol_source], OnGlobal, Format Table,
				File StrCat[folder, folder2, "Qjoule_tot.txt"] ];
		}
	}
	
	{ Name ResistivityTauIfcc; NameOfPostProcessing ResistTau;
		Operation {
			Print[ bField[Vol_coil], OnGlobal, Format Table,
				File StrCat[folder, folder2, "B_HT0.txt"] ];
		  For i In {1:nTests}
			Print[ rhoCu_HT0~{i}[Vol_coil], OnGlobal, Format Table, 
				File StrCat[folder, folder2, Sprintf("rhoCu_ht0%g.txt",i)] ]; 
			Print[ rho_HT0~{i}[Vol_coil], OnGlobal, Format Table,
				File StrCat[folder, folder2, Sprintf("rho_ht0%g.txt",i)] ];
			Print[ tau_HT0~{i}[Vol_coil], OnGlobal, Format Table,
				File StrCat[folder, folder2, Sprintf("tau_ht0%g.txt",i)] ]; 
		  EndFor
		}
	}
	
	{ Name HeatCapacity_T3B; NameOfPostProcessing T3B;
		Operation {
			Print[ Tht0[Vol_coil], OnGlobal, Format Table,
				File StrCat[folder, folder2, Sprintf("T_ht0_bg%g.txt",initB)] ];
				
			Print[ CvCu_ht0[Vol_coil], OnGlobal, Format Table,
				File StrCat[folder, folder2, Sprintf("CvCu_ht0_bg%g.txt",initB)] ];
			Print[ CvNb3Sn_ht0[Vol_coil], OnGlobal, Format Table,
				File StrCat[folder, folder2, Sprintf("CvSC_ht0_bg%g.txt",initB)] ];
			Print[ CvG10_ht0[Vol_coil], OnGlobal, Format Table,
				File StrCat[folder, folder2, Sprintf("CvG10_ht0_bg%g.txt",initB)] ];
			Print[ Cv_Mix_ht0[Vol_coil], OnGlobal, Format Table,
				File StrCat[folder, folder2, Sprintf("CvMix_ht0_bg%g.txt",initB)] ];
				
			Print[ kCu_ht0[Vol_coil], OnGlobal, Format Table,
				File StrCat[folder, folder2, Sprintf("kCu_ht0_bg%g.txt",initB)] ];
			Print[ kMix_ht0[Vol_coil], OnGlobal, Format Table,
				File StrCat[folder, folder2, Sprintf("kMix_ht0_bg%g.txt",initB)] ];
		}
	}
	
}

